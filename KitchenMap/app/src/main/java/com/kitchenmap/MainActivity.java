/*
 * Copyright (C) 2013 Manuel Peinado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kitchenmap;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.kitchenmap.messenger.BaseActivity;
import com.kitchenmap.messenger.fcm.FirebaseBackgroundService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * This activity showcases the most common way to use the ImageLayout class,
 * in which all the children are specified in XML
 */

public class MainActivity extends BaseActivity {

    public static Animation blink_animation;
    Animation animation;

    DBHelper DB;

    public static ArrayList<_ImageView_> _Rooms_;
    ArrayList<_ImageView_> Rooms;

    ImageView meetingRoom, gmRoom, conferenceRoom, cpoRoom, diningRoom, doctorsRoom, velocity, webdoc;
    String function__;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startService(new Intent(this, FirebaseBackgroundService.class));

        if (MySingleton.getInstance().isConnected(this)) {
            MySingleton.getInstance().showLoadingPopup(this, "Fetching Profile - Please Wait");
            function__ = "EmployeeList";
            final GetProfile asyncTask = new GetProfile(getApplicationContext(), new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    JSONArray arr = null;
                    try {
                        arr = ((JSONObject) result).getJSONArray("EmployeeListResult");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (!MySingleton.getInstance().setArray(arr)) {
                        new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                        "If the problem persists, please contact us at support@webdoc.com.pk")
                                .show();
                        return;
                    }

                    function__ = "KitchenItems";
                    final GetProfile asyncTask = new GetProfile(getApplicationContext(), new AsyncResponse() {
                        @Override
                        public void processFinish(Object result) {
                            JSONArray arr = null;
                            try {
                                arr = ((JSONObject) result).getJSONArray("KitchenItemsResult");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (!MySingleton.getInstance().setArray(arr)) {
                                new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Something went wrong!")
                                        .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                                "If the problem persists, please contact us at support@webdoc.com.pk")
                                        .show();
                                return;
                            }
                            MySingleton.getInstance().dismissLoadingPopup();
                        }
                    }, function__ + "__" + "0");
                    asyncTask.execute();
                }
            }, function__ + "__" + "0");
            asyncTask.execute();
        } else {
            Snackbar.make(findViewById(android.R.id.content), "No Network Available", Snackbar.LENGTH_LONG)
                    .setActionTextColor(Color.RED)
                    .show();
        }

        DB = new DBHelper(getApplicationContext());
        Rooms = new ArrayList<>();
        _Rooms_ = Rooms;

        animation = new AlphaAnimation(1, 0);
        animation.setDuration(2500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);

        blink_animation = animation;

        setUpEverything();

        _Rooms_.add(new _ImageView_(meetingRoom, "meetingRoom"));
        _Rooms_.add(new _ImageView_(gmRoom, "gmRoom"));
        _Rooms_.add(new _ImageView_(conferenceRoom, "conferenceRoom"));
        _Rooms_.add(new _ImageView_(cpoRoom, "cpoRoom"));
        _Rooms_.add(new _ImageView_(diningRoom, "diningRoom"));
        _Rooms_.add(new _ImageView_(doctorsRoom, "doctorsRoom"));
        _Rooms_.add(new _ImageView_(velocity, "velocity"));
        _Rooms_.add(new _ImageView_(webdoc, "webdoc"));

        //DB.deleteNotifications();
        //DB.deleteMessages();

        DB.checkPendingOrders();
    }

    private void setUpEverything() {
        meetingRoom = (ImageView) findViewById(R.id.meetingRoom_blinker);
        meetingRoom.setVisibility(View.INVISIBLE);
        meetingRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), l.class).putExtra("_location_", "meetingRoom"));
            }
        });

        gmRoom = (ImageView) findViewById(R.id.gmRoom_blinker);
        gmRoom.setVisibility(View.INVISIBLE);
        gmRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), l.class).putExtra("_location_", "gmRoom"));
            }
        });

        conferenceRoom = (ImageView) findViewById(R.id.conferenceRoom_blinker);
        conferenceRoom.setVisibility(View.INVISIBLE);
        conferenceRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), l.class).putExtra("_location_", "conferenceRoom"));
            }
        });

        cpoRoom = (ImageView) findViewById(R.id.cpoRoom_blinker);
        cpoRoom.setVisibility(View.INVISIBLE);
        cpoRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), l.class).putExtra("_location_", "cpoRoom"));
            }
        });

        diningRoom = (ImageView) findViewById(R.id.diningRoom_blinker);
        diningRoom.setVisibility(View.INVISIBLE);
        diningRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), l.class).putExtra("_location_", "diningRoom"));
            }
        });

        doctorsRoom = (ImageView) findViewById(R.id.doctorsRoom_blinker);
        doctorsRoom.setVisibility(View.INVISIBLE);
        doctorsRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), l.class).putExtra("_location_", "doctorsRoom"));
            }
        });

        velocity = (ImageView) findViewById(R.id.velocity_blinker);
        velocity.setVisibility(View.INVISIBLE);
        velocity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), l.class).putExtra("_location_", "velocity"));
            }
        });

        webdoc = (ImageView) findViewById(R.id.webdoc_blinker);
        webdoc.setVisibility(View.INVISIBLE);
        webdoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), l.class).putExtra("_location_", "webdoc"));
            }
        });

    }

    @Override
    public void onServiceConnected() {
        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient("kitchen123@webdoc.com.pk");
        }
    }

    @Override
    public void onServiceDisconnected() {
    }


    public class _ImageView_ {
        ImageView ImageView;
        String Name;

        _ImageView_(ImageView im, String n) {
            ImageView = im;
            Name = n;
        }

    }

    private ImageView imOfName(String name) {
        ImageView im = null;
        for (int i = 0; i < Rooms.size(); i++) {
            if (Rooms.get(i).Name.equals(name)) {
                im = Rooms.get(i).ImageView;
                break;
            }
        }
        return im;
    }

    public static void newOrder(String location) {
        ImageView im = null;
        for (int i = 0; i < _Rooms_.size(); i++) {
            if (_Rooms_.get(i).Name.equals(location)) {
                im = _Rooms_.get(i).ImageView;
                im.startAnimation(blink_animation);
                break;
            }
        }
    }
}
