package com.kitchenmap;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by faheem on 03/08/2017.
 */

public class GetProfile extends android.os.AsyncTask<Void, Void, Object> {
    //ProgressDialog loading;
    public AsyncResponse delegate = null;//Call back interface
    String a;
    Context Contextt_;

    public GetProfile(Context c, AsyncResponse asyncResponse, String a) {
        Contextt_=c;
        this.Contextt_ = c;
        delegate = asyncResponse;//Assigning call back interfacethrough constructor
        this.a = a;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //loading = ProgressDialog.show(getApplicationContext(),"Fetching...","Profile...",false,false);
    }

    @Override
    protected void onPostExecute(Object s) {
        super.onPostExecute(s);
        //loading.dismiss();
        delegate.processFinish(s);
    }

    @Override
    protected Object doInBackground(Void... params) {
        WCFHandler wcf = new WCFHandler(Contextt_);
        String[] parts = a.split("__");

        if (parts[1].equals("1")) {
            return new Object();
        } else {
            JSONObject s = wcf.GetJsonResult(parts[0]);
            return s;
        }
    }
}