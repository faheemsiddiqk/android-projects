package com.kitchenmap.messenger.fcm;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.kitchenmap.DBHelper;
import com.kitchenmap.MainActivity;
import com.kitchenmap.R;
import com.kitchenmap.messenger.AudioPlayer;
import com.sinch.android.rtc.messaging.Message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import br.com.goncalves.pugnotification.notification.PugNotification;

/**
 * Created by faheem on 04/12/2017.
 */

public class FirebaseBackgroundService extends Service {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("WebDocInternal");
    DBHelper DB;

    Intent intent;
    ActivityManager am;
    List<ActivityManager.RunningTaskInfo> taskInfo;
    String classname;
    Boolean _b_;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        DB = new DBHelper(getApplicationContext());

        intent = new Intent(getApplicationContext(), MainActivity.class);

        am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        // Read from the database
        Query query = myRef.orderByChild("receiver").equalTo("kitchen123@webdoc.com.pk");

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    DataSnapshot child = dataSnapshot;

                    String key = child.getKey();

                    String id = (String) child.child("id").getValue();
                    String message = (String) child.child("message").getValue();
                    String sender = (String) child.child("sender").getValue();
                    String receiver = (String) child.child("receiver").getValue();
                    String shown = (String) child.child("shown").getValue();
                    String timestamp = (String) child.child("timestamp").getValue();

                    if (shown == null)
                        return;

                    if (shown.equals("0")) {
                        String body = message.split("###")[0];
                        String location = message.split("###")[1];

                        DB.insertMessage(body, sender, timestamp, location);

                        Map notification = new HashMap<>();
                        notification.put("shown", "1");
                        myRef.child(key).updateChildren(notification);

                        showNofication(sender, message);

                        try {
                            MainActivity.newOrder(location);
                        } catch (NullPointerException e) {
                        }
                    }

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d("", "changed");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("", "removed");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d("", "moved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("", "cancelled");
            }
        });

    }

    void showNofication(String sender, String message) {

        int count = 0;
        String a = sender;

        DB.insertNotification_("", a, "Message");
        count = DB.getNotification_(a, "Message");

        Intent stopIntent = new Intent("my.awersome.string.name");
        stopIntent.putExtra("id", count);
        PendingIntent stopPi = PendingIntent.getBroadcast(getApplicationContext(), 4, stopIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        if (message.equals("***")) {
            PugNotification.with(getApplicationContext())
                    .load()
                    .identifier(count)
                    .title("Buzzer")
                    .message(sender)
                    .bigTextStyle("From : " + sender)
                    .smallIcon(R.mipmap.ic_launcher)
                    .largeIcon(R.mipmap.ic_launcher)
                    .priority(Notification.PRIORITY_HIGH)
                    .autoCancel(true)
                    .flags(Notification.DEFAULT_VIBRATE)
                    .vibrate(new long[]{100})
                    .dismiss(stopPi)
                    .simple()
                    .build();
        } else {
            PugNotification.with(getApplicationContext())
                    .load()
                    .identifier(count)
                    .title("New Order")
                    .message(sender)
                    .bigTextStyle("From : " + sender)
                    .smallIcon(R.mipmap.ic_launcher)
                    .largeIcon(R.mipmap.ic_launcher)
                    .priority(Notification.PRIORITY_HIGH)
                    .autoCancel(true)
                    .flags(Notification.DEFAULT_VIBRATE)
                    .vibrate(new long[]{100})
                    .dismiss(stopPi)
                    .simple()
                    .build();
        }

        new AudioPlayer(getApplicationContext()).playMsg();
    }


    private Message newMessage(final String id, final String body, final String receipient, final String sender, final String timeStamp) {
        Date finalD = null;
        SimpleDateFormat mFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.US);
        try {
            Date d1 = (Date) mFormatter.parse(timeStamp);
            finalD = d1;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final Date finalD1 = finalD;
        Message m = new Message() {
            @Override
            public String getMessageId() {
                return id;
            }

            @Override
            public Map<String, String> getHeaders() {
                return null;
            }

            @Override
            public String getTextBody() {
                return body;
            }

            @Override
            public List<String> getRecipientIds() {
                return new ArrayList<>(Arrays.asList(receipient));
            }

            @Override
            public String getSenderId() {
                return sender;
            }

            @Override
            public Date getTimestamp() {
                return finalD1;
            }
        };
        return m;
    }

    private String userName() {
        SharedPreferences sharedpref = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        String uname = sharedpref.getString("UserName", "0");
        String pass = sharedpref.getString("Password", "0");

        return uname;
    }

}
