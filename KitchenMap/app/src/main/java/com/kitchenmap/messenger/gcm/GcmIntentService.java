package com.kitchenmap.messenger.gcm;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Notification;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.kitchenmap.DBHelper;
import com.kitchenmap.messenger.SinchService;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.SinchHelpers;

import java.util.List;

public class GcmIntentService extends IntentService implements ServiceConnection {

    private Intent mIntent;

    private SinchService.SinchServiceInterface mSinchServiceInterface;

    private static SinchService.SinchServiceInterface mSinchServiceInterfacee;

    DBHelper DB;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (SinchHelpers.isSinchPushIntent(intent)) {
            mIntent = intent;
            connectToService();
        } else {
            GcmBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    private void connectToService() {
        getApplicationContext().bindService(new Intent(this, SinchService.class), this, Context.BIND_AUTO_CREATE);
        //SinchService.SinchServiceInterface mSinchServiceInterface
    }

    protected void onServiceConnected() {
        // for subclasses
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (mIntent == null) {
            return;
        }

        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            onServiceConnected();
        }

        DB = new DBHelper(this);
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo;
        String classname;
        Boolean _b_;

        Notification n;

        if (SinchHelpers.isSinchPushIntent(mIntent)) {
            SinchService.SinchServiceInterface sinchService = (SinchService.SinchServiceInterface) iBinder;
            if (sinchService != null) {
                NotificationResult result = sinchService.relayRemotePushNotificationPayload(mIntent);
                // handle result, e.g. show a notification or similar
                System.out.println("******" + result);
                if (result.isMessage()) {
                        /*int count = 0;
                        String a = result.getMessageResult().getSenderId();

                        DB.insertNotification_("", a, "Message");
                        count = DB.getNotification_(a, "Message");

                        Intent stopIntent = new Intent("my.awersome.string.name");
                        stopIntent.putExtra("id", count);
                        PendingIntent stopPi = PendingIntent.getBroadcast(getApplicationContext(), 4, stopIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                        PugNotification.with(getApplicationContext())
                                .load()
                                .identifier(count)
                                .title("Message")
                                .message(result.getMessageResult().getSenderId())
                                .bigTextStyle("From : " + result.getMessageResult().getSenderId())
                                .smallIcon(R.mipmap.ic_launcher)
                                .largeIcon(R.mipmap.ic_launcher)
                                .priority(Notification.PRIORITY_HIGH)
                                .autoCancel(true)
                                .flags(Notification.DEFAULT_VIBRATE)
                                .vibrate(new long[]{100})
                                .dismiss(stopPi)
                                .simple()
                                .build();

                        new AudioPlayer(getApplicationContext()).playMsg();*/

                    }

                GcmBroadcastReceiver.completeWakefulIntent(mIntent);
                mIntent = null;
            }
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
    }
}