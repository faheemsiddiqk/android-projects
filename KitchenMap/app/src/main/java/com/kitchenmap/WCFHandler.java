package com.kitchenmap;

/**
 * Created by ustad on 4/24/2017.
 */

import android.content.Context;
import android.os.StrictMode;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;


public class WCFHandler {

    static String webUrl = "https://mgtapp.webddocsystems.com/Service1.svc/";
    static Context context;

    public WCFHandler(Context c) {
        context = c;
    }

    public static JSONObject GetJsonResult(String functionName) {
        CertificateFactory cf = null;
        HttpsURLConnection urlConnection = null;
        try {
            try {
                cf = CertificateFactory.getInstance("X.509");
            } catch (CertificateException e) {
                e.printStackTrace();
            }
            JSONObject jarray = null;
            try {
                InputStream caInput = context.getResources().openRawResource(R.raw.webddocsystems);
                Certificate ca = null;
                try {
                    ca = cf.generateCertificate(caInput);
                    System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
                } catch (CertificateException e) {
                    e.printStackTrace();
                }
                String keyStoreType = KeyStore.getDefaultType();
                KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", ca);

                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                tmf.init(keyStore);

                SSLContext context = SSLContext.getInstance("TLS");
                context.init(null, tmf.getTrustManagers(), null);

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                URL url = new URL(webUrl + functionName);
                urlConnection = (HttpsURLConnection) url.openConnection();
                urlConnection.setSSLSocketFactory(context.getSocketFactory());
                urlConnection.setRequestMethod("GET");
                urlConnection.setConnectTimeout(20000);
                urlConnection.connect();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String jsonData = "";

                jsonData = bufferedReader.readLine();
                jarray = new JSONObject(jsonData);

            } catch (NullPointerException e) {
            }

            //  Toast.makeText(context,jsonData,Toast.LENGTH_SHORT).show();
            return jarray;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            //   Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (ProtocolException e) {
            e.printStackTrace();
            //   Toast.makeText(null, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            //    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
            //    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        return null;
    }
}