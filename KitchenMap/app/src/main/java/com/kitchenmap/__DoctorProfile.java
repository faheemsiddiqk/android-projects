package com.kitchenmap;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by faheem on 2/2/17.
 */
public class __DoctorProfile extends Activity {

    ArrayList<ListData> main;
    double ScreenSize;

    ImageView profile_img;
    EditText fullNameET;
    ListView listView;
    Button button_avalibility_slots;

    JSONArray arr_profiles, arr_items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.___popup_doctor_profile);

        arr_profiles = MySingleton.getInstance().getArray(0);
        arr_items = MySingleton.getInstance().getArray(1);

        main = new ArrayList<>();

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        ScreenSize = Math.sqrt(x + y);

        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .75));

        String sender = getIntent().getStringExtra("_sender_");
        String data = getIntent().getStringExtra("_data_");

        profile_img = (ImageView) findViewById(R.id.toolbarImage);
        fullNameET = (EditText) findViewById(R.id.NameET);
        listView = (ListView) findViewById(R.id.listView);
        button_avalibility_slots = (Button) findViewById(R.id.button_avalibility_slots);

        JSONArray arr = MySingleton.getInstance().getArray(0);

        String link = "";
        for (int i = 0; i < arr.length(); i++) {
            try {
                if (arr.getJSONObject(i).getString("Email").equals(sender)) {
                    link = arr.getJSONObject(i).getString("imglink");
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Glide.with(getApplicationContext())
                .load(link)
                .into(profile_img);

        fullNameET.setText(sender.split("@")[0]);
        if (data.equals("***")) {
        } else {
            String[] items = data.split("\n");

            for (int i = 0; i < items.length; i++) {
                String count = items[i].split(" of ")[0];
                String item = items[i].split(" of ")[1];
                main.add(new ListData(count, getLink_i(item)));
            }

            listView.setAdapter(new SampleAdapter());
            Helper.getListViewSize(listView);
        }
        button_avalibility_slots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DBHelper(getApplicationContext()).orderDelivered(getIntent().getStringExtra("_id_"));
                finish();
            }
        });
    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getApplicationContext(), R.layout.___list_item_kitchen, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

            holder.Name.setText(item.Name);
            Glide.with(getApplicationContext())
                    .load(item.Drawable)
                    .override(getWidth(), getHeight())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder.ProgressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.imageView);

            return convertView;

        }
    }

    private static class ViewHolder {
        private View view;

        private TextView Name;
        private ImageView imageView;
        private ProgressBar ProgressBar;

        private ViewHolder(View view) {
            this.view = view;
            Name = (TextView) view.findViewById(R.id.Name);
            imageView = (ImageView) view.findViewById(R.id.image);
            ProgressBar = (android.widget.ProgressBar) view.findViewById(R.id.progressBar);
        }
    }

    private class ListData {
        private String Name;
        private String Drawable;

        public ListData(String n, String d) {
            Name = n;
            Drawable = d;
        }
    }

    private int getWidth() {
        if (ScreenSize > 4.3)
            return 400;//400;
        else
            return 280;
    }

    private int getHeight() {
        if (ScreenSize > 4.3)
            return 280;//280;
        else
            return 180;
    }

    private String getLink_p(String name) {
        JSONObject inner;
        String link = "";

        for (int i = 0; i < arr_profiles.length(); i++) {
            try {
                inner = arr_profiles.getJSONObject(i);
                if (inner.getString("Email").equals(name)) {
                    link = inner.getString("Imglink");
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return link;
    }

    private String getLink_i(String name) {
        JSONObject inner;
        String link = "";

        for (int i = 0; i < arr_items.length(); i++) {
            try {
                inner = arr_items.getJSONObject(i);
                if (inner.getString("LName").equals(name)) {
                    link = inner.getString("LImageLink");
                    break;
                } else if (inner.getString("RName").equals(name)) {
                    link = inner.getString("RImageLink");
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return link;
    }
}
