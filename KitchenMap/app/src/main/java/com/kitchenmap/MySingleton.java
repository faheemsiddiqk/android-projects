package com.kitchenmap;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.kitchenmap.messenger.BaseActivity;

import org.json.JSONArray;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by faheem on 03/07/2017.
 */

public class MySingleton extends BaseActivity {

    static Context Contextt_;

    private static final MySingleton ourInstance = new MySingleton();

    public ArrayList<JSONArray> MainList = new ArrayList<JSONArray>();

    private SweetAlertDialog pDialog = null;

    public static MySingleton getInstance() {
        return ourInstance;
    }

    public static MySingleton getInstance(Context c) {
        Contextt_ = c;
        return ourInstance;
    }

    private MySingleton() {
    }


    public JSONArray getArray(int index) {
        try {
            return MainList.get(index);
        } catch (IndexOutOfBoundsException exception) {
            //showDialog(Contextt_);
            return null;
        }
    }

    public boolean setArray(JSONArray list) {
        if (list == null)
            return false;
        MainList.add(list);
        return true;
    }

    public void updateArray(int index, JSONArray list) {
        try {
            MainList.set(index, list);
        } catch (IndexOutOfBoundsException e) {
            MainList.add(list);
        }
    }

    public boolean isConnected(Context c) {
        ConnectivityManager connectivityManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifiInfo != null && wifiInfo.isConnected()) || (mobileInfo != null && mobileInfo.isConnected())) {
            return true;
        } else {
            return false;
        }
    }

    public void showLoadingPopup(Context con, String a) {
        pDialog = new SweetAlertDialog(con, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#cb3439"));
        pDialog.setTitleText(a);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void dismissLoadingPopup() {
        if (pDialog != null)
            try {
                pDialog.dismiss();
            } catch (IllegalArgumentException e) {
                return;
            }
    }

}



