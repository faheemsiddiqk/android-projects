package com.kitchenmap;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.kitchenmap.messenger.BaseActivity;

import java.util.ArrayList;
import java.util.List;


public class l extends BaseActivity {

    // list of data items
    ListView listView;

    private List<ListData> mDataList;

    // declare the color generator and drawable builder
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private TextDrawable.IBuilder mDrawableBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_l);

        mDataList = new ArrayList<ListData>();

        mDrawableBuilder = TextDrawable.builder()
                .beginConfig()
                .withBorder(4)
                .endConfig()
                .round();


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .8));

        // init the list view and its adapter
        listView = (ListView) findViewById(R.id.listView);

        loadInfo();
        listView.setAdapter(new SampleAdapter());


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i_ = new Intent(getApplicationContext(), __DoctorProfile.class);
                i_.putExtra("_id_", mDataList.get(position).Id);
                i_.putExtra("_sender_",mDataList.get(position).Sender);
                i_.putExtra("_data_",mDataList.get(position).Data);
                startActivityForResult(i_,1010);
            }
        });
    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public ListData getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(l.this, R.layout.___list_item_layout, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

            // provide support for selected state
            TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Sender.toUpperCase().charAt(0)), mColorGenerator.getColor(item.Sender));
            holder.imageView.setImageDrawable(drawable);

            holder.textViewU.setText(item.Sender);
            holder.textViewU.setSelected(true);
            holder.textViewB.setText(item.Data);
            holder.textViewD.setText(item.Timestamp);

            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;
        private ImageView imageView;
        private TextView textViewU;
        private TextView textViewB;
        private TextView textViewD;

        private ViewHolder(View view) {
            this.view = view;
            imageView = (ImageView) view.findViewById(R.id.imageViewdList);
            textViewU = (TextView) view.findViewById(R.id.textViewdList_Username);
            textViewB = (TextView) view.findViewById(R.id.textViewdList_Body);
            textViewD = (TextView) view.findViewById(R.id.textViewdList_Date);
        }
    }

    private static class ListData {

        private String Id;
        private String Data;
        private String Sender;
        private String Timestamp;

        public ListData(String id, String data, String sender, String timestamp) {
            this.Id = id;
            this.Data = data;
            this.Sender = sender;
            this.Timestamp = timestamp;
        }
    }
    private void loadInfo() {
        DBHelper db = new DBHelper(this);
        List<List<String>> messages = db.getAllMessages(getIntent().getStringExtra("_location_"));

        for (int i = 0; i < messages.size(); i++) {
            String id = messages.get(i).get(0);
            String body = messages.get(i).get(1);
            String sender = messages.get(i).get(2);
            String timestamp = messages.get(i).get(3);

            mDataList.add(new ListData(id,body,sender,timestamp));
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1010) {
            mDataList = new ArrayList<ListData>();
            loadInfo();
            listView.setAdapter(new SampleAdapter());
        }
    }
}