package com.kitchenmap;

/**
 * Created by faheem on 25/07/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.animation.Animation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Kitchen.db";
    public static final String CONTACTS_TABLE_NAME = "contacts";
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_NAME = "name";

    private HashMap hp;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table MessageList " +
                        "(id integer primary key, body varchar, sender varchar, timestamp varchar, location varchar, delivered varchar)"
        );
        db.execSQL(
                "create table AllNotifications " +
                        "(id integer primary key, name varchar, type varchar)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS MessageList");
        onCreate(db);
    }

    public void deleteMessages() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("MessageList", "", new String[]{});
        db.close();
    }
    public void deleteNotifications() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("AllNotifications", "", new String[]{});
        db.close();
    }

    public boolean insertMessage(String body, String sender, String timestamp, String location) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("body", body);
        contentValues.put("sender", sender);
        contentValues.put("timestamp", timestamp);
        contentValues.put("location", location);
        contentValues.put("delivered", "0");

        //db.insert("MessageList", null, contentValues);
        db.insertWithOnConflict("MessageList", null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);

        db.close();
        return true;
    }

    public boolean messagesRead(String name, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("read", value);

        db.update("PatientList", contentValues, "name = ?", new String[]{name});
        db.close();
        return true;
    }

    public boolean messages__Read(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("read", "1");

        db.update("MessageList", contentValues, "sender = ?", new String[]{name});
        db.close();
        return true;
    }

    public List<List<String>> getAllMessages(String location) {
        List<List<String>> listOfLists = new ArrayList<List<String>>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from MessageList where delivered='0' and location='" + location + "'", null);
        res.moveToFirst();


        while (res.isAfterLast() == false) {
            String id = res.getString(res.getColumnIndex("id"));
            String body = res.getString(res.getColumnIndex("body"));
            String sender = res.getString(res.getColumnIndex("sender"));
            String timestamp = res.getString(res.getColumnIndex("timestamp"));

            listOfLists.add(new ArrayList<String>(Arrays.asList(id, body, sender, timestamp)));
            res.moveToNext();
        }
        db.close();
        return listOfLists;
    }

    public boolean insertNotification_(String id, String name, String type) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("name", name);
        contentValues.put("type", type);

        db.insert("AllNotifications", null, contentValues);
        db.close();
        return true;
    }

    public void deleteNotification_(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("AllNotifications", "id = ?", new String[]{id});
    }

    public int getNotification_(String name, String type) {
        int ans = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select MAX(id) from AllNotifications WHERE name = ? and type = ?", new String[]{name, type});
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            try {
                ans = Integer.parseInt(res.getString(0));
            } catch (NumberFormatException e) {
                ans = 0;
            }

            res.moveToNext();
        }
        db.close();
        return ans;
    }

    public List<Integer> getAllNotifications_(String name) {
        List<Integer> listOfLists = new ArrayList<Integer>();
        int id = 0;

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from AllNotifications WHERE name = ?", new String[]{name});
        res.moveToFirst();


        while (res.isAfterLast() == false) {
            id = Integer.parseInt(res.getString(res.getColumnIndex("id")));

            listOfLists.add(id);
            res.moveToNext();
        }
        db.close();
        return listOfLists;
    }

/*    "create table MessageList " +
            "(id integer primary key, body varchar, sender varchar, timestamp varchar, location varchar, delivered varchar)"*/

    public void checkPendingOrders() {
        ArrayList<MainActivity._ImageView_> Rooms = MainActivity._Rooms_;
        Animation blink_animation = MainActivity.blink_animation;
        SQLiteDatabase db = this.getReadableDatabase();
        for (int i = 0; i < Rooms.size(); i++) {
            Cursor res = db.rawQuery("select * from MessageList WHERE location = ? and delivered = ?", new String[]{Rooms.get(i).Name, "0"});
            res.moveToFirst();

            while (res.isAfterLast() == false) {
                Rooms.get(i).ImageView.startAnimation(blink_animation);
                break;
                //res.moveToNext();
            }
        }
        db.close();
    }

    public boolean orderDelivered(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("delivered", "1");

        db.update("MessageList", contentValues, "id = ?", new String[]{id});
        db.close();

        checkPendingOrders_();

        return true;
    }

    private void checkPendingOrders_() {
        SQLiteDatabase db = this.getReadableDatabase();

        for (int i = 0; i < MainActivity._Rooms_.size(); i++) {
            int total = 0, totaldelivered = 0;
            Cursor res_ = db.rawQuery("select * from MessageList WHERE location = ?", new String[]{MainActivity._Rooms_.get(i).Name});
            res_.moveToFirst();

            while (res_.isAfterLast() == false) {
                total++;
                res_.moveToNext();
            }

            Cursor res = db.rawQuery("select * from MessageList WHERE location = ? and delivered = ?", new String[]{MainActivity._Rooms_.get(i).Name, "1"});
            res.moveToFirst();

            while (res.isAfterLast() == false) {
                totaldelivered++;
                res.moveToNext();
            }
            if (total == totaldelivered)
                MainActivity._Rooms_.get(i).ImageView.clearAnimation();
        }
        db.close();
    }
}