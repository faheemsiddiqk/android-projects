package com.webdoc.internalapp.fragment.navigation;

/**
 * @author msahakyan
 */

public interface NavigationManager {

    void showFragmentGeneralProfile(String title);

    void showFragmentInsuranceProducts(String title);

    void showFragmentDoctorList(String title);
}
