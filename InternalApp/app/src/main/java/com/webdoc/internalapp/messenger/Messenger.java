package com.webdoc.internalapp.messenger;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.messaging.Message;
import com.sinch.android.rtc.messaging.MessageClient;
import com.sinch.android.rtc.messaging.MessageClientListener;
import com.sinch.android.rtc.messaging.MessageDeliveryInfo;
import com.sinch.android.rtc.messaging.MessageFailureInfo;
import com.webdoc.internalapp.AsyncResponse;
import com.webdoc.internalapp.DBHelper;
import com.webdoc.internalapp.GetNetwork;
import com.webdoc.internalapp.LauncherActivity;
import com.webdoc.internalapp.MySingleton;
import com.webdoc.internalapp.R;
import com.webdoc.internalapp.WCFHandler;
import com.webdoc.internalapp.fragment._2_FragmentChatBox;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import br.com.goncalves.pugnotification.notification.PugNotification;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class Messenger extends BaseActivity implements MessageClientListener {

    private static final String TAG = Messenger.class.getSimpleName();

    private MessageAdapter mMessageAdapter;
    private static MessageAdapter mMessageAdapter_;
    private EditText mTxtTextBody;
    String recipient, me;
    private static String recipient_;

    String jarr;
    WCFHandler wcf;
    DBHelper DB;

    String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout._messaging);

        DB = new DBHelper(this);
        wcf = new WCFHandler(this);

        findViewById(R.id.message_body).setVisibility(View.VISIBLE);

        Toolbar topToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(topToolBar);
        // topToolBar.setLogo(R.drawable.logotoolbar);
        // topToolBar.setLogoDescription("TEST");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ((TextView) findViewById(R.id.dName)).setText(getIntent().getStringExtra("UserName"));

        recipient = getIntent().getStringExtra("UserName");
        userName = getIntent().getStringExtra("MyName");

        recipient_ = recipient;
        me = MySingleton.getInstance().getUserId();
        if (me.equals(""))
            me = userName;
        Toast.makeText(this, me, Toast.LENGTH_LONG).show();

        MySingleton.getInstance().setRecipient(recipient);
        List<Integer> datalist = DB.getAllNotifications_(recipient);

        for (int i = 0; i < datalist.size(); i++) {
            PugNotification.with(getApplicationContext()).cancel(datalist.get(i));
            DB.deleteNotification_(datalist.get(i) + "");
        }

        mTxtTextBody = (EditText) findViewById(R.id.txtTextBody);

        mMessageAdapter = new MessageAdapter(this, recipient, MySingleton.getInstance().getUserId());
        mMessageAdapter_ = mMessageAdapter;
        ListView messagesList = (ListView) findViewById(R.id.lstMessages);

        loadInfo();
        messagesList.setAdapter(mMessageAdapter_);

        if (!MySingleton.getInstance(this).isConnectedButton())
            ((Button) findViewById(R.id.m_btnSend)).setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        //super.onNewIntent(intent);
        ((TextView) findViewById(R.id.dName)).setText(intent.getStringExtra("Name"));

        recipient = intent.getStringExtra("UserName");
        recipient_ = recipient;

        MySingleton.getInstance().setRecipient(recipient);
        List<Integer> datalist = DB.getAllNotifications_(recipient);

        for (int i = 0; i < datalist.size(); i++) {
            PugNotification.with(getApplicationContext()).cancel(datalist.get(i));
            DB.deleteNotification_(datalist.get(i) + "");
        }
        mMessageAdapter = new MessageAdapter(this, recipient, MySingleton.getInstance().getUserId());
        mMessageAdapter_ = mMessageAdapter;
        ListView messagesList = (ListView) findViewById(R.id.lstMessages);

        loadInfo();

        messagesList.setAdapter(mMessageAdapter_);
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    protected void onStart() {
        super.onStart();
        invalidateOptionsMenu();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        try {
            _2_FragmentChatBox.getInstance().onViewCreated(false);
        } catch (NullPointerException e) {
            /*String mn = getIntent().getStringExtra("MyName");
            MySingleton.getInstance().setUserId(mn);
            boolean a = MySingleton.getInstance().MainList.size() != 0;
            MySingleton.getInstance().setMainAvailable(true);*/
            //startActivity(new Intent(this,_MainActivity.class).putExtra("ListAvailable",a));
            startActivity(new Intent(this, LauncherActivity.class));
        }
        finish();
    }

    @Override
    public void onDestroy() {
        if (getSinchServiceInterface() != null) {
            getSinchServiceInterface().removeMessageClientListener(this);
        }
        super.onDestroy();
    }

    @Override
    public void onServiceConnected() {
        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(userName());
        }
        ((Button) findViewById(R.id.m_btnSend)).setVisibility(View.VISIBLE);
        getSinchServiceInterface().addMessageClientListener(this);
        //Toast.makeText(getApplicationContext(),getSinchServiceInterface().getUserName(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceDisconnected() {
    }

    private void sendMessage() {
        //recipient = getIntent().getStringExtra("UserName");
        final String textBody = mTxtTextBody.getText().toString();
        if (recipient.isEmpty()) {
            Toast.makeText(this, "No recipient added", Toast.LENGTH_SHORT).show();
            return;
        }
        if (textBody.isEmpty()) {
            Toast.makeText(this, "No text message", Toast.LENGTH_SHORT).show();
            return;
        }

        GetNetwork async = new GetNetwork(Messenger.this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                Boolean a = (Boolean) result;
                if (!a) {
                    new SweetAlertDialog(Messenger.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No Network")
                            .show();
                    return;
                } else {
                    String message = getSinchServiceInterface().getUserId() + " : " + textBody;
                    if (recipient.equals("system@webdoc.com.pk")) {
                        getSinchServiceInterface().sendSystemFirebase(message, UUID.randomUUID().toString());
                        Message m = newMessage(UUID.randomUUID().toString(), message, recipient, getSinchServiceInterface().getUserId(), new Date().toString());
                        onMessageSent(m);
                        mTxtTextBody.setText("");
                    } else {
                        getSinchServiceInterface().sendFirebase(recipient, textBody, UUID.randomUUID().toString());
                        Message m = newMessage(UUID.randomUUID().toString(), textBody, recipient, getSinchServiceInterface().getUserId(), new Date().toString());
                        onMessageSent(m);
                        mTxtTextBody.setText("");
                    }
                }
            }
        }, "");
        async.execute();
    }

    @Override
    public void onIncomingMessage(MessageClient client, Message message) {
        if (message.getSenderId().equals(MySingleton.getInstance().getUserId()) && message.getTextBody().equals("<Initialize>"))
            return;
        if (message.getSenderId().equals(recipient)) {
            mMessageAdapter.addMessage(message, MessageAdapter.DIRECTION_INCOMING);

            String id = message.getMessageId();
            String body = message.getTextBody();
            String recipient = message.getRecipientIds() + "";
            recipient = recipient.replace("[", "").replace("]", "");
            String sender = message.getSenderId();
            String timestamp = message.getTimestamp() + "";
            String direction = MessageAdapter.DIRECTION_INCOMING + "";

            DB.insertMessage(id, body, recipient, sender, timestamp, direction);
        }
    }

    public void onMessageSent(Message message) {
        mMessageAdapter.addMessage(message, MessageAdapter.DIRECTION_OUTGOING);

        String id = message.getMessageId();
        String body = message.getTextBody();
        String recipient = message.getRecipientIds() + "";
        recipient = recipient.replace("[", "").replace("]", "");
        String sender = message.getSenderId();
        String timestamp = message.getTimestamp() + "";
        String direction = MessageAdapter.DIRECTION_OUTGOING + "";

        DB.insertMessage(id, body, recipient, sender, timestamp, direction);
    }

    @Override
    public void onMessageSent(MessageClient client, Message message, String recipientId) {
        if (recipient.equals(MySingleton.getInstance().getUserId()) && message.getTextBody().equals("<Initialize>"))
            return;
        mMessageAdapter.addMessage(message, MessageAdapter.DIRECTION_OUTGOING);

        String id = message.getMessageId();
        String body = message.getTextBody();
        String recipient = message.getRecipientIds() + "";
        recipient = recipient.replace("[", "").replace("]", "");
        String sender = message.getSenderId();
        String timestamp = message.getTimestamp() + "";
        String direction = MessageAdapter.DIRECTION_OUTGOING + "";

        DB.insertMessage(id, body, recipient, sender, timestamp, direction);
    }

    @Override
    public void onShouldSendPushData(MessageClient client, Message message, List<PushPair> pushPairs) {
        // Left blank intentionally
    }

    @Override
    public void onMessageFailed(MessageClient client, Message message,
                                MessageFailureInfo failureInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("Sending failed: ")
                .append(failureInfo.getSinchError().getMessage());

        Toast.makeText(this, sb.toString(), Toast.LENGTH_LONG).show();
        Log.d(TAG, sb.toString());
    }

    @Override
    public void onMessageDelivered(MessageClient client, MessageDeliveryInfo deliveryInfo) {
        Log.d(TAG, "onDelivered");
    }

    public void sendMessage(View view) {
        sendMessage();
    }

    private void loadInfo() {

        DBHelper DB = new DBHelper(this);
        Message m;

        List<List<String>> messages = DB.getAllMessages();

        for (int i = 0; i < messages.size(); i++) {
            String id = messages.get(i).get(0);
            String body = messages.get(i).get(1);
            String recipient_ = messages.get(i).get(2);
            String sender = messages.get(i).get(3);
            String timestamp = messages.get(i).get(4);

            if ((recipient.equals(recipient_) && (me.equals(sender) || me.equals(recipient_))) || recipient.equals(sender) && (me.equals(sender) || me.equals(recipient_))) {
                m = newMessage(id, body, recipient_, sender, timestamp); //final String id, final String body, final String receipient, final String sender, final String timeStamp
                String a = m.getTimestamp() + "";
                mMessageAdapter.addMessage(m, Integer.parseInt(messages.get(i).get(5)));
            }
        }

    }

    Date finalD;
    SimpleDateFormat mFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.US);

    public Message newMessage(final String id, final String body, final String receipient, final String sender, final String timeStamp) {

        try {
            Date d1 = (Date) mFormatter.parse(timeStamp);
            finalD = d1;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Message m = new Message() {
            @Override
            public String getMessageId() {
                return id;
            }

            @Override
            public Map<String, String> getHeaders() {
                return null;
            }

            @Override
            public String getTextBody() {
                return body;
            }

            @Override
            public List<String> getRecipientIds() {
                return new ArrayList<>(Arrays.asList(receipient));
            }

            @Override
            public String getSenderId() {
                return sender;
            }

            @Override
            public Date getTimestamp() {
                return finalD;
            }
        };
        return m;
    }

    public static void updateList(Message m) {
        if (m.getSenderId().equals(recipient_))
            onMessageReceived(m);
    }

    private static void onMessageReceived(Message m) {
        mMessageAdapter_.addMessage(m, MessageAdapter.DIRECTION_INCOMING);
    }

    private String userName() {
        SharedPreferences sharedpref = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        String uname = sharedpref.getString("UserName", "0");
        String pass = sharedpref.getString("Password", "0");

        return uname;
    }
}
