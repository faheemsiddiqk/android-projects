package com.webdoc.internalapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.webdoc.internalapp.AsyncResponse;
import com.webdoc.internalapp.GetProfile;
import com.webdoc.internalapp.MySingleton;
import com.webdoc.internalapp.R;
import com.webdoc.internalapp.messenger.Messenger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class _2_FragmentChatBox extends Fragment {

    static ImageView im;
    int i_old;
    boolean b_old;

    private static final String KEY_MOVIE_TITLE = "key_title";
    String status;

    Intent message_int;
    // list of data items
    ListView listView;

    private ArrayList<ListData> mDataList;

    JSONArray jarr;
    ToggleSwitch toggleSwitch;

    // declare the color generator and drawable builder
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private TextDrawable.IBuilder mDrawableBuilder;

    private static _2_FragmentChatBox FragmentDoctorList = new _2_FragmentChatBox();
    static boolean first_time = true;

    boolean show_popup = true;

    public _2_FragmentChatBox() {
        // Required empty public constructor
    }

    public static _2_FragmentChatBox newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            FragmentDoctorList.setArguments(args);
        } catch (IllegalStateException e) {
            FragmentDoctorList = new _2_FragmentChatBox();
            FragmentDoctorList.setArguments(args);
        }

        return FragmentDoctorList;
    }

    public static _2_FragmentChatBox getInstance() {
        return FragmentDoctorList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.__fragment_2_fragment_chatbox, container, false);
    }

    View _v_;
    SampleAdapter listAdapter;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        message_int = new Intent(getActivity(), Messenger.class);
        mDrawableBuilder = TextDrawable.builder()
                .beginConfig()
                .withBorder(4)
                .endConfig()
                .round();

        listView = (ListView) view.findViewById(R.id.listViewdList);

        toggleSwitch = (ToggleSwitch) view.findViewById(R.id.status__);
        toggleSwitch.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                onViewCreated(false);
            }
        });

        onViewCreated(false);

        _v_ = view;
    }

    public void onViewCreated(Boolean a) throws NullPointerException {
        status = (1 - toggleSwitch.getCheckedTogglePosition()) + "";
        listAdapter = new SampleAdapter();
        listView.setAdapter(listAdapter);

        if (a) {
            if (show_popup) {
                MySingleton.getInstance().showLoadingPopup(getActivity(), "Fetching Employee's List - Please Wait");
                show_popup = false;
            }

            GetProfile asyncTask = new GetProfile(getActivity(), new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    /*try {
                        jarr = new JSONArray("[[\"Tahawur Abbas\",\"Khaleeq\",\"03005103515\",\"drtahawur@webdoc.com.pk\",\"\",\"Pakistan\",\"Family Medicine\",\"Prevention is better than cure\",\"0\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2017\\/02\\/drr.jpg\"],[\"Sana\",\"Akhlaque\",\"03347325013\",\"drsana@webdoc.com.pk\",\"2.9375\",\"PAKISTAN\",\"General Medicine\",\"\\\"Life is so prescious and Prevention is better than cure\\\"\",\"1\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/drsana.jpg\"],[\"Test\",\"Doctor\",\"1234567890\",\"testdoctor@webdoc.com.pk\",\"\",\"Pakistan\",\"Dentist\",\"This is a message.\",\"0\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/choos_a_doctor.png\"],[\"Muhammad \",\"Sohail\",\"03314677479\",\"drsohail@webdoc.com.pk\",\"4\",\"Pakistan\",\"General Medicine\",\"\",\"1\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/Sohail.jpg\"],[\"Raalah\",\"Ghaffar\",\"03365187573\",\"DrRaalah@webdoc.com.pk\",\"\",\"Pakistan\",\"Internal Medicine\",\"\",\"1\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/drraalah.jpg\"],[\"Saira\\u000d\\u000a\",\"Tahawur\\u000d\\u000a\",\"03329143354\",\"\",\"\",\"Pakistan\\u000d\\u000a\",\"Internal Medicine\",\"Hello\",\"0\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2017\\/02\\/dr.jpg\"],[\"Seemab \",\"Khalid\",\"03348458829\",\"drseemab@webdoc.com.pk\",\"\",\"Pakistan\",\"Internal Medicine\",\"everyday holds the possibility of a miracle\",\"0\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/Seemab.jpg\"],[\"Neelam\",\"Shahzadi\",\"03355867137\",\"drneelam@webdoc.com.pk\",\"\",\"Pakistan\",\"General Medicine\",\"\",\"0\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/Neelem.jpg\"]]");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    if (jarr == null) {
                        MySingleton.getInstance().dismissLoadingPopup();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                        "If the problem persists, please contact us at support@webdoc.com.pk")
                                .show();
                        return;
                    }
                    try {
                        jarr = ((JSONObject) result).getJSONArray("EmployeeListResult");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    MySingleton.getInstance().updateArray(7, jarr);
                    MySingleton.getInstance().dismissLoadingPopup();
                    dataAvailable();

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            if (MySingleton.getInstance().isConnectedButton()) {
                                message_int.putExtra("UserName", mDataList.get(position).UserName);
                                //message_int.putExtra("Status", mDataList.get(position).Status);
                                startActivity(message_int);
                            } else {
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Something went wrong!")
                                        .setContentText("No Network")
                                        .show();
                            }
                        }
                    });

                }
            }, "EmployeeList" + "__" + "0");
            asyncTask.execute();
        } else {
            dataAvailable();
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (MySingleton.getInstance().isConnectedButton()) {
                        message_int.putExtra("UserName", mDataList.get(position).UserName + "");
                        startActivity(message_int);
                    } else {
                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("No Network")
                                .show();
                    }
                }
            });
        }
    }

    void dataAvailable() {
        jarr = MySingleton.getInstance().getArray(1);
        if (jarr.length() == 0 || jarr == null)
            return;
        JSONObject jobj;

        listAdapter.addEntry(new ListData("WebDoc", "system@webdoc.com.pk", "_1010_"));

        for (int i = 0; i < jarr.length(); i++) {
            try {
                jobj = jarr.getJSONObject(i);
                String CompanyName = jobj.getString("CompanyName");
                String Email = jobj.getString("Email");
                String ID = jobj.getString("Id");

                if (!Email.equals(MySingleton.getInstance().getUserId()))
                    if (status.equals("1")) {
                        if (CompanyName.equals("WebDoc"))
                            listAdapter.addEntry(new ListData(CompanyName, Email, ID));
                    } else if (status.equals("0")) {
                        if (CompanyName.equals("Velocity"))
                            listAdapter.addEntry(new ListData(CompanyName, Email, ID));
                    }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private class SampleAdapter extends BaseAdapter {

        SampleAdapter() {
            mDataList = new ArrayList<ListData>();
        }

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public ListData getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void addEntry(ListData d) {
            if (!mDataList.contains(d)) {
                mDataList.add(d);
                notifyDataSetChanged();
            }

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.___list_item_dlist, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final ListData item = getItem(position);

            TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.UserName.toUpperCase().charAt(0)), mColorGenerator.getColor(item.UserName));
            holder.imageView.setImageDrawable(drawable);
            // UrlImageViewHelper.setUrlDrawable(holder.imageView, item.Path);

            holder.textView.setText(item.UserName);
            holder.textView.setSelected(true);

            return convertView;
        }
    }

    private static class ViewHolder {
        private View view;
        private ImageView imageView;
        private TextView textView;

        private ViewHolder(View view) {
            this.view = view;
            imageView = (ImageView) view.findViewById(R.id.imageViewdList);
            textView = (TextView) view.findViewById(R.id.textViewdList);
        }
    }

    private static class ListData {

        //["Sana", "Akhlaque", "03347325013", "drsana@webdoc.com.pk", "3.05", "PAKISTAN", "General Medicine", "\"Life is so prescious and Prevention is better than cure\"", "0"],

        private String CompanyName;
        private String UserName;
        private String ID;

        public ListData(String cn, String un, String id) {
            this.CompanyName = cn;
            this.UserName = un;
            this.ID = id;
        }
    }

}
