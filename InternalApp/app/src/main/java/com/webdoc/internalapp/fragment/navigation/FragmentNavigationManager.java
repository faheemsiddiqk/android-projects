package com.webdoc.internalapp.fragment.navigation;

import android.annotation.SuppressLint;
import android.support.v4.BuildConfig;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.webdoc.internalapp.R;
import com.webdoc.internalapp._MainActivity;
import com.webdoc.internalapp.fragment._2_FragmentChatBox;
import com.webdoc.internalapp.fragment._1_FragmentGeneralProfile;
import com.webdoc.internalapp.fragment._3_FragmentKitchen;


/**
 * @author msahakyan
 */

public class FragmentNavigationManager implements NavigationManager {

    private static FragmentNavigationManager sInstance;

    private FragmentManager mFragmentManager;
    private _MainActivity mActivity;

    public static FragmentNavigationManager obtain(_MainActivity activity) {
        if (sInstance == null) {
            sInstance = new FragmentNavigationManager();
        }
        sInstance.configure(activity);
        return sInstance;
    }

    private void configure(_MainActivity activity) {
        mActivity = activity;
        mFragmentManager = mActivity.getSupportFragmentManager();
    }

    @Override
    public void showFragmentGeneralProfile(String title) {
        showFragment(_1_FragmentGeneralProfile.newInstance(title) , true);
    }
    @Override
    public void showFragmentInsuranceProducts(String title) {
        showFragment(_3_FragmentKitchen.newInstance(title), true);
    }
    @Override
    public void showFragmentDoctorList(String title) {
        showFragment(_2_FragmentChatBox.newInstance(title),true);
    }

    private void showFragment(Fragment fragment, boolean allowStateLoss) {
        FragmentManager fm = mFragmentManager;

        @SuppressLint("CommitTransaction")
        FragmentTransaction ft = fm.beginTransaction()
            .replace(R.id.container, fragment);

        ft.addToBackStack(null);

        if (allowStateLoss || !BuildConfig.DEBUG) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }

        fm.executePendingTransactions();
    }
}
