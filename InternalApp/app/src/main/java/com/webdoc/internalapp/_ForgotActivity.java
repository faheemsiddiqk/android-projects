package com.webdoc.internalapp;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsMessage;
import android.text.InputFilter;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.webdoc.internalapp.messenger.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.Random;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by faheem on 16/11/2017.
 */

public class _ForgotActivity extends BaseActivity {

    Boolean isCodeReceived = false;

    CountDownTimer cdt;

    EditText email, pin, password, cpassword;
    TextView timer;
    ToggleSwitch toggleSwitch;

    String codeSent, codeReceived;

    private void showView(View root) {
        root.animate().alpha(1.0f).setDuration(2000);
        root.setVisibility(View.VISIBLE);
    }

    private void hideView(final View root) {
        root.animate().alpha(0.0f).setDuration(2000).withEndAction(new Runnable() {
            @Override
            public void run() {
                root.setVisibility(View.GONE);
            }
        });

    }

    private String formatTimespan(long totalSeconds) {
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private String generateRandomNumber() {
        Random rand = new Random();
        String num = "";
        for (int i = 0; i < 4; i++) {
            num += rand.nextInt(10);
        }
        //Toast.makeText(this, num, Toast.LENGTH_SHORT).show();
        return num;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotactivity);

        cdt = new CountDownTimer(301000, 1000) {
            public void onTick(long millisUntilFinished) {
                timer.setText(formatTimespan(millisUntilFinished / 1000));
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                timer.setText("Please Try Again Your Code Has Expired");
            }
        };

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(_ForgotActivity.this,
                    new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS},
                    1);
        else {
            IntentFilter filter = new IntentFilter();
            filter.addAction(Telephony.Sms.Intents.SMS_RECEIVED_ACTION);
            registerReceiver(new MyReceiver(), filter);
            startEverything();
        }
    }

    String username;

    public void startEverything() {

        MySingleton.getInstance().isConnected(this);

        final InputFilter[] filter11 = new InputFilter[]{new InputFilter.LengthFilter(11)};
        final InputFilter[] filter99 = new InputFilter[]{new InputFilter.LengthFilter(99)};

        toggleSwitch = (ToggleSwitch) findViewById(R.id.status__);
        toggleSwitch.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                if (position == 0) {
                    /*((EditText) findViewById(R.id.email)).setText("");
                    ((EditText) findViewById(R.id.email)).setInputType(InputType.TYPE_CLASS_PHONE);
                    ((EditText) findViewById(R.id.email)).setHint("Phone Number");
                    ((EditText) findViewById(R.id.email)).setFilters(filter11);*/
                } else {
                    /*((EditText) findViewById(R.id.email)).setText("");
                    ((TextView) findViewById(R.id.email)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    ((EditText) findViewById(R.id.email)).setHint("Email Address");
                    ((EditText) findViewById(R.id.email)).setFilters(filter99);*/
                }
            }
        });

        email = (EditText) findViewById(R.id.email);

        pin = (EditText) findViewById(R.id.txt_pin_entry);
        pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        final PinEntryEditText pinEntry = (PinEntryEditText) findViewById(R.id.txt_pin_entry);
        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    if (str.toString().equals(codeSent)) {
                        if (timer.getText().toString().equals("Please Try Again Your Code Has Expired")) {
                            pinEntry.setText("");
                            new SweetAlertDialog(_ForgotActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("Please Try Again Your Code Has Expired")
                                    .show();
                            return;
                        }
                        Toast.makeText(_ForgotActivity.this, "SUCCESS", Toast.LENGTH_SHORT).show();
                        hideView(findViewById(R.id.RStep1));
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                showView(findViewById(R.id.RStep2));
                            }
                        }, 2000);

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(email.getWindowToken(), 0);

                    } else {
                        Toast.makeText(_ForgotActivity.this, "FAIL", Toast.LENGTH_SHORT).show();
                        pinEntry.setText(null);
                    }
                }
            });
        }

        timer = (TextView) findViewById(R.id.timer);

        password = (EditText) findViewById(R.id.password);
        cpassword = (EditText) findViewById(R.id.confirmpassword);
    }

    public void mainSend(final View view) {
        System.out.println("***** " + email.getText());

        username = email.getText() + "";
        if (!username.contains("@"))
            username += "@webdoc.com.pk";

        if (username.equals("")) {
            new SweetAlertDialog(_ForgotActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText("Empty Field")
                    .show();
            return;
        }

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);


        codeSent = generateRandomNumber();

        GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                if (result == null) {
                    new SweetAlertDialog(_ForgotActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("Please try again later")
                            .show();
                    return;
                }
                String res = null;
                try {
                    res = ((JSONObject) result).getJSONArray("MessageSendResult").getJSONObject(0).getString("Response");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (res.equals("Success")) {
                    ((Button) view).setText("Resend");
                    showView(findViewById(R.id.step2));
                    cdt.start();
                } else if (result.equals("Fail")) {
                    new SweetAlertDialog(_ForgotActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("Please try again later")
                            .show();
                    return;
                } else {
                }

            }
        }, "MessageSend/" + username + "/" + Uri.encode("**WebDoc Reset Code is:") + codeSent + "**" + "__" + "0");
        asyncTask.execute();

    }

    public void openLogin(View view) {
        finish();
    }

    public void mainSubmit(View view) {
        String pass = password.getText() + "";
        String cpass = cpassword.getText() + "";

        if (pass.equals(cpass)) {

            if (pass.length() < 7) {
                new SweetAlertDialog(_ForgotActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Something went wrong!")
                        .setContentText("Password length must be greater than 7")
                        .show();
                return;
            }

            GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    JSONObject result_ = (JSONObject) result;
                    if (result_ == null) {
                        new SweetAlertDialog(_ForgotActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("Please try again later")
                                .show();
                        return;
                    }
                    String res = null;
                    try {
                        res = ((JSONObject) result).getJSONArray("ForgotPasswordResult").getJSONObject(0).getString("Response");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (res.equals("Success")) {
                        new SweetAlertDialog(_ForgotActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Success!")
                                .setContentText("Please login using your new password")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                        finish();
                                    }
                                })
                                .show();
                        return;
                    } else if (res.equals("Fail")) {
                        new SweetAlertDialog(_ForgotActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("Please try again later")
                                .show();
                        return;
                    } else {
                    }

                }
            }, "ForgotPassword/" + username + "/" + pass + "__" + "0");
            asyncTask.execute();

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    IntentFilter filter = new IntentFilter();
                    filter.addAction(Telephony.Sms.Intents.SMS_RECEIVED_ACTION);
                    registerReceiver(new MyReceiver(), filter);
                } else {
                }
                startEverything();
                return;
            }
        }
    }

    class MyReceiver extends BroadcastReceiver {
        public MyReceiver() {
        }

        String str, receiver, message;

        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle bundle = intent.getExtras();
            SmsMessage[] recievedMsgs = null;
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                recievedMsgs = new SmsMessage[pdus.length];
                for (int i = 0; i < pdus.length; i++) {
                    recievedMsgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    str = "SMS from " + recievedMsgs[i].getOriginatingAddress() +
                            " : " + recievedMsgs[i].getMessageBody().toString();
                    receiver = recievedMsgs[i].getOriginatingAddress();
                    message = recievedMsgs[i].getMessageBody().toString();
                }
            }

            if (message.contains("**WebDoc Reset Code is:")) {
                codeReceived = message.replaceAll("\\*\\*", "");
                codeReceived = codeReceived.split(":")[1];

                if (codeReceived.equals(codeSent)) {
                    isCodeReceived = true;
                    pin.setText(codeReceived);
                }
            }

            //Toast.makeText(getApplication(), str, Toast.LENGTH_LONG).show();

        }
    }
}
