package com.webdoc.internalapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import com.sinch.android.rtc.SinchError;
import com.webdoc.internalapp.messenger.BaseActivityLauncher;
import com.webdoc.internalapp.messenger.SinchService;

public class LauncherActivity extends BaseActivityLauncher implements SinchService.StartFailedListener {

    @Override
    protected void onServiceConnected() {

        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().setStartListener(LauncherActivity.this);
        }
        if (MySingleton.getInstance().isConnectedButton(LauncherActivity.this)) {
            checkInfo();
        } else {
            startActivity(new Intent(LauncherActivity.this, _Splash.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_launcher);
    }

    private void checkInfo() {
        SharedPreferences sharedpref = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        String uname = sharedpref.getString("UserName", "0");
        String pass = sharedpref.getString("Password", "0");

        if (uname != "0" && pass != "0") {
            if (!getSinchServiceInterface().isStarted()) {
                getSinchServiceInterface().startClient(uname);
            }
            MySingleton.getInstance().setUserId(uname);
            startActivity(new Intent(this, _MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        } else {
            startActivity(new Intent(this, _Splash.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(getApplicationContext(), "NOT STARTED", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {
        Toast.makeText(getApplicationContext(), "STARTED", Toast.LENGTH_LONG).show();
    }
}
