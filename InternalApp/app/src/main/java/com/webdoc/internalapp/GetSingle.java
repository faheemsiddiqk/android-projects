package com.webdoc.internalapp;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by faheem on 16/08/2017.
 */

public class GetSingle extends android.os.AsyncTask<Void, Void, Object> {
    //ProgressDialog loading;
    public AsyncResponse delegate = null;//Call back interface
    String a;
    Context Contextt_;

    public GetSingle(Context c, AsyncResponse asyncResponse, String a) {
        this.Contextt_ = c;
        delegate = asyncResponse;//Assigning call back interfacethrough constructor
        this.a = a;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        MySingleton.getInstance().showLoadingPopup(Contextt_, "Please Wait");
    }

    @Override
    protected void onPostExecute(Object s) {
        super.onPostExecute(s);
        MySingleton.getInstance().dismissLoadingPopup();
        delegate.processFinish(s);
    }

    @Override
    protected Object doInBackground(Void... params) {
        WCFHandler wcf = new WCFHandler(Contextt_);
        String[] parts = a.split("__");

        if (parts[1].equals("1")) {
            //String str = wcf.GetJsonResult(parts[0]);
            return new Object();
        } else {
            JSONObject s = wcf.GetJsonResult(parts[0]);
            return s;
        }
    }
}