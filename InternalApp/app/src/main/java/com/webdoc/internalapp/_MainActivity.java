package com.webdoc.internalapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.webdoc.internalapp.adapter.CustomExpandableListAdapter;
import com.webdoc.internalapp.datasource.ExpandableListDataSource;
import com.webdoc.internalapp.dialog.QustomDialogBuilder;
import com.webdoc.internalapp.fragment.navigation.FragmentNavigationManager;
import com.webdoc.internalapp.fragment.navigation.NavigationManager;
import com.webdoc.internalapp.messenger.BaseActivity;
import com.webdoc.internalapp.messenger.fcm.FirebaseBackgroundService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class _MainActivity extends BaseActivity {

    private int lastExpandedPosition = -1;
    String function__;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    private String[] items;

    private ExpandableListView mExpandableListView;
    private ExpandableListAdapter mExpandableListAdapter;
    private List<Integer> mExpandableListIcon;
    private List<String> mExpandableListTitle;
    private NavigationManager mNavigationManager;

    private Map<String, List<String>> mExpandableListData;

    Toolbar topToolBar;

    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private TextDrawable.IBuilder mDrawableBuilder;
    static private TextDrawable.IBuilder mDrawableBuilderr;

    LinearLayout navigationView;
    TextView tName, tEmail;
    ImageView tImage;

    static TextView tNamee, tEmaill;
    static ImageView tImagee;

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, 15).show();
            } else {
                Log.e("MainActivity", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startService(new Intent(this, FirebaseBackgroundService.class));

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        double screenInches = Math.sqrt(x + y);
        MySingleton.getInstance().setScreenSize(screenInches);
        Toast.makeText(getApplicationContext(), "Screen inches : " + screenInches, Toast.LENGTH_LONG).show();

        checkPlayServices();

        if (MySingleton.getInstance().MainList.size() == 0) {
            if (MySingleton.getInstance().isConnected(this)) {
                MySingleton.getInstance().showLoadingPopup(this, "Fetching Profile - Please Wait");
                function__ = "GetUserProfile/" + MySingleton.getInstance().getUserId();
                final GetProfile asyncTask = new GetProfile(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        if ((JSONObject) result == null) {
                            MySingleton.getInstance().dismissLoadingPopup();
                            new SweetAlertDialog(_MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            finish();
                                        }
                                    })
                                    .show();
                            return;
                        }
                        JSONArray jsn = null;//wcf.GetJsonResult("GetPatientProfile/" + MySingleton.getInstance().getUserId());
                        try {
                            jsn = ((JSONObject) result).getJSONArray("GetUserProfileResult");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        MySingleton.getInstance().setArray(jsn);

                        function__ = "EmployeeList";
                        final GetProfile asyncTask = new GetProfile(getApplicationContext(), new AsyncResponse() {
                            @Override
                            public void processFinish(Object result) {
                                if ((JSONObject) result == null) {
                                    MySingleton.getInstance().dismissLoadingPopup();
                                    new SweetAlertDialog(_MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Something went wrong!")
                                            .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                                    "If the problem persists, please contact us at support@webdoc.com.pk")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    finish();
                                                }
                                            })
                                            .show();
                                    return;
                                }
                                JSONArray arr = null;
                                try {
                                    arr = ((JSONObject) result).getJSONArray("EmployeeListResult");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                MySingleton.getInstance().setArray(arr);

                                function__ = "KitchenItems";
                                final GetProfile asyncTask = new GetProfile(getApplicationContext(), new AsyncResponse() {
                                    @Override
                                    public void processFinish(Object result) {
                                        if ((JSONObject) result == null) {
                                            MySingleton.getInstance().dismissLoadingPopup();
                                            new SweetAlertDialog(_MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                    .setTitleText("Something went wrong!")
                                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                            finish();
                                                        }
                                                    })
                                                    .show();
                                            return;
                                        }
                                        JSONArray arr = null;
                                        try {
                                            arr = ((JSONObject) result).getJSONArray("KitchenItemsResult");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        MySingleton.getInstance().setArray(arr);

                                        if (savedInstanceState == null) {
                                            selectFirstItemAsDefault();
                                        }
                                        JSONArray j1 = MySingleton.getInstance().getArray(0);
                                        try {
                                            if (!(j1.get(0).equals("") && j1.get(0).equals(""))) {
                                                tName.setText(j1.get(0) + " " + j1.get(1));
                                                tEmail.setText(MySingleton.getInstance().getUserId().split("@")[0]);
                                                TextDrawable drawable = mDrawableBuilder.build(String.valueOf(j1.get(0).toString().toUpperCase().charAt(0)), ContextCompat.getColor(getApplicationContext(), R.color.line_divider));
                                                tImage.setImageDrawable(drawable);
                                            } else {
                                                tImage.setImageResource(R.drawable.logowebdoc);
                                                tName.setText("");
                                                tEmail.setText("");
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        MySingleton.getInstance().dismissLoadingPopup();
                                    }
                                }, function__ + "__" + "0");
                                asyncTask.execute();
                            }
                        }, function__ + "__" + "0");
                        asyncTask.execute();
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            } else {
                Snackbar.make(findViewById(android.R.id.content), "No Network Available", Snackbar.LENGTH_LONG)
                        .setActionTextColor(Color.RED)
                        .show();
            }
        }

        mDrawableBuilder = TextDrawable.builder()
                .beginConfig()
                .withBorder(4)
                .endConfig()
                .round();

        mDrawableBuilderr = mDrawableBuilder;

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mActivityTitle = getTitle().toString();

        mExpandableListView = (ExpandableListView)

                findViewById(R.id.navList);

        mNavigationManager = FragmentNavigationManager.obtain(this);

        initItems();

        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.nav_header, null, false);
        mExpandableListView.addHeaderView(listHeaderView);
        tName = listHeaderView.findViewById(R.id.header_Name);
        tEmail = listHeaderView.findViewById(R.id.header_Email);
        tImage = listHeaderView.findViewById(R.id.headerImage);

        tNamee = tName;
        tEmaill = tEmail;
        tImagee = tImage;

        mExpandableListIcon = ExpandableListDataSource.getIcons(this);
        mExpandableListData = ExpandableListDataSource.getData(this);
        mExpandableListTitle = new ArrayList(mExpandableListData.keySet());

        addDrawerItems();

        setupDrawer();

        topToolBar = (Toolbar) findViewById(R.id.toolbar);
        MySingleton.getInstance().setToolbar(topToolBar);

        setSupportActionBar(topToolBar);
        topToolBar.setLogo(R.drawable.logotoolbar);
        topToolBar.setTitle("");
        topToolBar.setSubtitle("");
        topToolBar.setLogoDescription("");

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeButtonEnabled(true);

        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener()

        {
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {

                if (groupPosition == 0) {
                    GetNetwork async = new GetNetwork(_MainActivity.this, new AsyncResponse() {
                        @Override
                        public void processFinish(Object result) {
                            Boolean a = (Boolean) result;
                            if (!a) {
                                new SweetAlertDialog(_MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Something went wrong!")
                                        .setContentText("No Network")
                                        .show();
                                return;
                            } else {
                                QustomDialogBuilder qustomDialogBuilder = new QustomDialogBuilder(_MainActivity.this).
                                        setTitle("Room Picker").
                                        setTitleColor("#CB3439").
                                        setDividerColor("#CB3439").
                                        setMessage("What is your location ?").
                                        setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

                                qustomDialogBuilder.setOrder("***");
                                qustomDialogBuilder.setSinchInterface(getSinchServiceInterface());
                                android.app.AlertDialog ad = qustomDialogBuilder.show();
                                qustomDialogBuilder.setDialog(ad);
                            }
                        }
                    }, "");
                    async.execute();
                } else if (groupPosition == 1) {
                    mNavigationManager.showFragmentGeneralProfile("General Profile");
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                } else if (groupPosition == 4) {
                    SharedPreferences sharedpref = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
                    sharedpref.edit().clear().commit();
                    MySingleton.getInstance().setBoolean(false);
                    startActivity(new Intent(getApplicationContext(), _Splash.class));
                    finish();
                    MySingleton.getInstance().MainList.clear();
                    getSinchServiceInterface().stopClient();
                    return true;
                } else if (groupPosition == 2) {
                    mNavigationManager.showFragmentDoctorList("Chat Box");
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                } else if (groupPosition == 3) {
                    mNavigationManager.showFragmentInsuranceProducts("Insurance Products");
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                } /*else if (groupPosition == 3) {
                    mNavigationManager.showFragmentCorporatePanel("Corporate Panels");
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    return true;
                }*/
                return false;
            }
        });

        mExpandableListView.setChildDivider(

                getResources().

                        getDrawable(R.drawable.list_divider));

        if (savedInstanceState == null && MySingleton.getInstance().MainList.size() != 0)

        {
            selectFirstItemAsDefault();
        }
        mExpandableListView.setGroupIndicator(null);
    }


    private void showDoctorList() {
        if (mNavigationManager != null) {
            String firstActionMovie = "DONTUPDATE";
            mNavigationManager.showFragmentDoctorList(firstActionMovie);
            //        getSupportActionBar().setTitle(firstActionMovie);
        }
    }

    private void selectFirstItemAsDefault() {
        if (mNavigationManager != null) {
            String firstActionMovie = "General Profile";
            mNavigationManager.showFragmentGeneralProfile(firstActionMovie);
            //        getSupportActionBar().setTitle(firstActionMovie);
        }
    }

    private void initItems() {
        items = getResources().getStringArray(R.array.Main);
    }

    private void addDrawerItems() {
        mExpandableListAdapter = new CustomExpandableListAdapter(this, mExpandableListIcon, mExpandableListTitle, mExpandableListData);
        mExpandableListView.setAdapter(mExpandableListAdapter);

        mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                    mExpandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
                getSupportActionBar().setTitle(mExpandableListTitle.get(groupPosition).toString());
            }
        });

        mExpandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                getSupportActionBar().setTitle("Drawer");
            }
        });

       /* mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                String selectedItem = ((List) (mExpandableListData.get(mExpandableListTitle.get(groupPosition))))
                        .get(childPosition).toString();
                getSupportActionBar().setTitle(selectedItem);

                if (items[1].equals(mExpandableListTitle.get(groupPosition))) {
                    if (childPosition == 0) {
                        mNavigationManager.showFragmentConditions(selectedItem);
                    }
                    if (childPosition == 1)
                        mNavigationManager.showFragmentAllergies(selectedItem);
                    if (childPosition == 2)
                        mNavigationManager.showFragmentMedications(selectedItem);
                    if (childPosition == 3)
                        mNavigationManager.showFragmentImmunizations(selectedItem);
                    if (childPosition == 4)
                        mNavigationManager.showFragmentProcedures(selectedItem);
                } else if (items[4].equals(mExpandableListTitle.get(groupPosition))) {
                    if (childPosition == 0)
                        mNavigationManager.showFragmentEmptyReports(selectedItem);
                } else {
                    throw new IllegalArgumentException("Not supported fragment type");
                }

                mDrawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });*/
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Drawer");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

    }

    public static void changeHeaderValues(Context c, String fn, String ln) {
        if (!(fn.equals("") && ln.equals(""))) {
            TextDrawable drawable = mDrawableBuilderr.build(String.valueOf(fn.toString().toUpperCase().charAt(0)), ContextCompat.getColor(c, R.color.line_divider));
            tImagee.setImageDrawable(drawable);
            tNamee.setText(fn + " " + ln);
            tEmaill.setText(MySingleton.getInstance().getUserId().split("@")[0]);
        } else {
            tImagee.setImageResource(R.drawable.logowebdoc);
            tNamee.setText("");
            tEmaill.setText("");
        }
    }
}
