package com.webdoc.internalapp;

import android.content.Context;

import java.io.IOException;

/**
 * Created by faheem on 24/08/2017.
 */

public class GetNetwork extends android.os.AsyncTask<Void, Void, Object> {
    //ProgressDialog loading;
    public AsyncResponse delegate = null;//Call back interface
    String a;
    Context Contextt_;

    public GetNetwork(Context c, AsyncResponse asyncResponse, String a) {
        this.Contextt_ = c;
        delegate = asyncResponse;//Assigning call back interfacethrough constructor
        this.a = a;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        MySingleton.getInstance().showLoadingPopup(Contextt_,"Please Wait");
    }

    @Override
    protected void onPostExecute(Object s) {
        super.onPostExecute(s);
        MySingleton.getInstance().dismissLoadingPopup();
        delegate.processFinish(s);
    }

    @Override
    protected Object doInBackground(Void... params) {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 172.217.9.142");
            int exitValue = ipProcess.waitFor();
            if (exitValue == 0){
                MySingleton.getInstance().dismissLoadingPopup();
                return true;}
            else {
                MySingleton.getInstance().dismissLoadingPopup();
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }
}