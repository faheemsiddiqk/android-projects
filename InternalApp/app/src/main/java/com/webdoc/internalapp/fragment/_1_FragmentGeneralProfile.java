package com.webdoc.internalapp.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.webdoc.internalapp.MySingleton;
import com.webdoc.internalapp.R;
import com.webdoc.internalapp._MainActivity;
import com.webdoc.internalapp.__PopupEditProfile;
import com.webdoc.internalapp.messenger.BaseActivityFragment;
import com.webdoc.internalapp.messenger.SinchService;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Calendar;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link _1_FragmentGeneralProfile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class _1_FragmentGeneralProfile extends BaseActivityFragment {

    SinchService.SinchServiceInterface sinch;
    boolean systemSent = false;

    @Override
    protected void onServiceConnected() {
        sinch = getSinchServiceInterface();

        Arr = MySingleton.getInstance().getArray(0);

        try {

            if (Arr.length() == 0) {
                b1.setText("Add Profile");
                setHasOptionsMenu(true);
            } else
                b1.setText("Edit Profile");


            _MainActivity.changeHeaderValues(getContext(), Arr.getJSONObject(0).getString("fname"), Arr.getJSONObject(0).getString("lname"));

            fn.setText(Arr.getJSONObject(0).getString("fname"));
            fn.setSelected(true);
            ln.setText(Arr.getJSONObject(0).getString("lname"));
            cnic.setText(Arr.getJSONObject(0).getString("cnic"));

            String _dob_ = Arr.getJSONObject(0).getString("dob");
            dob.setText(_dob_);
            String currentD = DateFormat.format("MMM d", Calendar.getInstance().getTimeInMillis()).toString();
            if (_dob_.contains(currentD)) {
                sendBirthday();
            }

            gender.setText(Arr.getJSONObject(0).getString("gender"));
            address.setText(Arr.getJSONObject(0).getString("address"));
            mobile.setText(Arr.getJSONObject(0).getString("cell"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static final String KEY_MOVIE_TITLE = "key_title";

    JSONArray Arr;
    TextView fn, ln, cnic, dob, gender, address, country, city, mobile;
    Button b1;

    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private TextDrawable.IBuilder mDrawableBuilder;

    LinearLayout navigationView;
    TextView tName, tEmail;
    ImageView tImage;
    View _v_;


    public _1_FragmentGeneralProfile() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment _1_FragmentGeneralProfile.
     */
    static _1_FragmentGeneralProfile fragmentGeneralProfile = new _1_FragmentGeneralProfile();

    public static _1_FragmentGeneralProfile newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            fragmentGeneralProfile.setArguments(args);
        } catch (IllegalStateException e) {
            fragmentGeneralProfile = new _1_FragmentGeneralProfile();
            fragmentGeneralProfile.setArguments(args);
        }
        return fragmentGeneralProfile;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.__fragment_1_fragmentgeneralprofile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        _v_ = view;

        mDrawableBuilder = TextDrawable.builder()
                .beginConfig()
                .withBorder(4)
                .endConfig()
                .round();


        fn = view.findViewById(R.id.GeneralProfile_FN);
        dob = view.findViewById(R.id.GeneralProfile_DOB);
        ln = view.findViewById(R.id.GeneralProfile_LN);
        cnic = view.findViewById(R.id.GeneralProfile_CNIC);
        gender = view.findViewById(R.id.GeneralProfile_Gender);
        address = view.findViewById(R.id.GeneralProfile_Address);
        country = view.findViewById(R.id.GeneralProfile_Country);
        city = view.findViewById(R.id.GeneralProfile_City);
        mobile = view.findViewById(R.id.GeneralProfile_Mobile);
        b1 = view.findViewById(R.id.editProfile);

        if (!MySingleton.getInstance().isConnectedButton()) {
            view.findViewById(R.id.editProfile).setVisibility(View.GONE);
        }

        ((Button) view.findViewById(R.id.editProfile)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_ = new Intent(getActivity(), __PopupEditProfile.class);
                i_.putExtra("Date", dob.getText().toString());
                i_.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(i_, 101);
            }
        });

        Arr = MySingleton.getInstance().getArray(0);

        try {

            if (Arr.length() == 0) {
                b1.setText("Add Profile");
                setHasOptionsMenu(true);
            } else
                b1.setText("Edit Profile");


            _MainActivity.changeHeaderValues(getContext(), Arr.getJSONObject(0).getString("fname"), Arr.getJSONObject(0).getString("lname"));

            fn.setText(Arr.getJSONObject(0).getString("fname"));
            fn.setSelected(true);
            ln.setText(Arr.getJSONObject(0).getString("lname"));
            cnic.setText(Arr.getJSONObject(0).getString("cnic"));

            String _dob_ = Arr.getJSONObject(0).getString("dob");
            dob.setText(_dob_);
            String currentD = DateFormat.format("MMM d", Calendar.getInstance().getTimeInMillis()).toString();
            if (_dob_.contains(currentD)) {
                sendBirthday();
            }


            gender.setText(Arr.getJSONObject(0).getString("gender"));
            address.setText(Arr.getJSONObject(0).getString("address"));
            mobile.setText(Arr.getJSONObject(0).getString("cell"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {

            if (resultCode == Activity.RESULT_OK) {
                MySingleton.getInstance().editPopup(getContext());
            } else if (resultCode == 50) {
                MySingleton.getInstance().addPopup(getContext());
            }
            Arr = MySingleton.getInstance().getArray(0);

            try {

                if (Arr.length() == 0) {
                    b1.setText("Add Profile");
                    setHasOptionsMenu(true);
                } else
                    b1.setText("Edit Profile");


                _MainActivity.changeHeaderValues(getContext(), Arr.getJSONObject(0).getString("fname"), Arr.getJSONObject(0).getString("lname"));

                fn.setText(Arr.getJSONObject(0).getString("fname"));
                fn.setSelected(true);
                ln.setText(Arr.getJSONObject(0).getString("lname"));
                cnic.setText(Arr.getJSONObject(0).getString("cnic"));
                dob.setText(Arr.getJSONObject(0).getString("dob"));
                gender.setText(Arr.getJSONObject(0).getString("gender"));
                address.setText(Arr.getJSONObject(0).getString("address"));
                mobile.setText(Arr.getJSONObject(0).getString("cell"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void sendBirthday() {
        if (!systemSent) {
            try {
                String message = "It's " + getSinchServiceInterface().getUserId().toUpperCase().split("@")[0] + "'s Birthday, Don't Forget To Wish Him A Happy Birthday";
                sinch.sendSystemFirebase(message, UUID.randomUUID().toString());
                systemSent = true;
            } catch (NullPointerException e) {
            }
        }
    }

}
