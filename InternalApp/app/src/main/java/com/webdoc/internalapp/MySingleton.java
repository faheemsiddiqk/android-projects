package com.webdoc.internalapp;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;

import com.webdoc.internalapp.messenger.BaseActivity;

import org.json.JSONArray;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.webdoc.internalapp.WCFHandler.context;

/**
 * Created by faheem on 03/07/2017.
 */

public class MySingleton extends BaseActivity {

    private String Location="";

    static Context Contextt_;

    private static final MySingleton ourInstance = new MySingleton();
    private boolean firstTime = true;
    private boolean MainAvailable = false;
    boolean isGeneralProfileComplete = false;
    private String mobileNo = "";
    public ArrayList<JSONArray> MainList = new ArrayList<JSONArray>();

    private double DoctorRating = -1;
    private String DoctorStatus = "1";

    private String ActivityName = "";
    private SweetAlertDialog pDialog = null;
    private String recipient = "";
    private Double ScreenSize;

    Toolbar toolbar;

    public static MySingleton getInstance() {
        return ourInstance;
    }

    public static MySingleton getInstance(Context c) {
        Contextt_ = c;
        return ourInstance;
    }

    private MySingleton() {
    }

    public void setScreenSize(Double SC){
        ScreenSize=SC;
    }

    public Double getScreenSize(){
        return ScreenSize;
    }

    //280,180 //400,280
    public int getWidth(){
        if(ScreenSize>4.3)
            return 400;
        else
            return 280;
    }

    public int getHeight(){
        if(ScreenSize>4.3)
            return 280;
        else
            return 180;
    }

    public void setLocation(String l){
        Location=l;
    }

    public String getLocation(){
        return Location;
    }

    public boolean getBoolean() {
        return firstTime;
    }

    public void setBoolean(Boolean ft) {
        firstTime = ft;
    }

    public boolean getGprofile() {
        return isGeneralProfileComplete;
    }

    public void setGprofile(Boolean igpc) {
        isGeneralProfileComplete = igpc;
    }

    public boolean getMainAvailable() {
        return MainAvailable;
    }

    public void setMainAvailable(Boolean ma) {
        MainAvailable = ma;
    }

    public String getUserId() {
        return mobileNo;
    }

    public void setUserId(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public JSONArray getArray(int index) {
        try {
            return MainList.get(index);
        } catch (IndexOutOfBoundsException exception) {
            //showDialog(Contextt_);
            return null;
        }
    }

    public void setArray(JSONArray list) {
        MainList.add(list);
    }

    public void updateArray(int index, JSONArray list) {
        try {
            MainList.set(index, list);
        } catch (IndexOutOfBoundsException e) {
            MainList.add(list);
        }
    }

    public double getDoctorRating() {
        return DoctorRating;
    }

    public void setDoctorRating(double rat) {
        this.DoctorRating = rat;
    }

    public String getDoctorStatus() {
        return DoctorStatus;
    }

    public void setDoctorStatus(String s) {
        this.DoctorStatus = s;
    }

    public String getActivityName() {
        return ActivityName;
    }

    public void setActivityName(String AN) {
        ActivityName = AN;
    }

    public boolean isConnected(Context context) {
        Contextt_ = context;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifiInfo != null && wifiInfo.isConnected()) || (mobileInfo != null && mobileInfo.isConnected())) {
            return true;
        } else {
            showDialog(context);
            return false;
        }
    }

    public boolean isConnectedButton() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifiInfo != null && wifiInfo.isConnected()) || (mobileInfo != null && mobileInfo.isConnected())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isConnectedButton(Context c) {
        ConnectivityManager connectivityManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifiInfo != null && wifiInfo.isConnected()) || (mobileInfo != null && mobileInfo.isConnected())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isConnectedNetwork(Context c) {
        //showLoadingPopup(c,"Please Wait");
        GetNetwork async = new GetNetwork(c, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {

            }
        }, "");
        return false;
    }

    public void showLoadingPopup(Context con, String a) {
        pDialog = new SweetAlertDialog(con, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#cb3439"));
        pDialog.setTitleText(a);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void dismissLoadingPopup() {
        if (pDialog != null)
            try {
                pDialog.dismiss();
            } catch (IllegalArgumentException e) {
                return;
            }
    }

    private void showDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Connect to a network or quit")
                .setCancelable(false)
                .setPositiveButton("Connect to WIFI/Refresh", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (!isConnected(context))
                            context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((Activity) context).finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void editPopup(Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Result!")
                .setContentText("Edited Successfully")
                .show();
    }

    public void addPopup(Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Result!")
                .setContentText("Added Successfully")
                .show();
    }

    public void setToolbar(Toolbar t) {
        toolbar = t;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public View getGroupView(ExpandableListView listView, int groupPosition) {
        long packedPosition = ExpandableListView.getPackedPositionForGroup(groupPosition);
        int flatPosition = listView.getFlatListPosition(groupPosition);
        int first = listView.getFirstVisiblePosition();
        return listView.getChildAt(groupPosition);
    }

    /*public String getNameFromUserName(String a) {
        try {
            JSONArray main = MainList.get(7);
            JSONObject item = null;
            String name = "";
            for (int i = 0; i < main.length(); i++) {
                try {
                    item = (JSONObject) main.get(i);
                    if (item.get(3).equals(a)) {
                        name = item.get(0) + " " + item.get(1);
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return name;
        } catch (IndexOutOfBoundsException e) {
            return "";
        }
    }*/

    public boolean checkRecipient(String name) {
        if (recipient.equals(name))
            return true;
        else
            return false;
    }

    public void setRecipient(String name) {
        recipient = name;
    }

}



