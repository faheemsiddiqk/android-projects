package com.webdoc.internalapp;

/**
 * Created by faheem on 03/08/2017.
 */

public interface AsyncResponse {
    void processFinish(Object result);
}