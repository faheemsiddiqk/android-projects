package com.webdoc.internalapp;


import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sinch.android.rtc.SinchError;
import com.webdoc.internalapp.messenger.BaseActivity;
import com.webdoc.internalapp.messenger.SinchService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class _Splash extends BaseActivity implements SinchService.StartFailedListener {

    @Override
    protected void onServiceConnected() {

        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().setStartListener(_Splash.this);
        }
    }

    ToggleSwitch toggleSwitch;

    RelativeLayout rl;

    private ImageView iv;
    private TextView et;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        checkInfo();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (false)
            ActivityCompat.requestPermissions(_Splash.this,
                    new String[]{},
                    1);
        else
            startEverything();

        /*new DBHelper(this).deletePatients();
        new DBHelper(this).deleteMessagess();

        for(int i=0;i<50;i++)
        new DBHelper(this).deleteNotification_(i+"");*/

    }

    public void startEverything() {

        toggleSwitch = (ToggleSwitch) findViewById(R.id.status__);

        rl = (RelativeLayout) findViewById(R.id.rl1);
        iv = (ImageView) findViewById(R.id.ivLogo);

        MySingleton.getInstance().isConnected(this);

        /*if (MySingleton.getInstance().isConnected(this)) {
            checkInfo();
        }*/

        if (MySingleton.getInstance().getBoolean()) {

            final Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink_animation);

            iv.startAnimation(startAnimation);
            iv.postDelayed(new Runnable() {
                public void run() {

                    final TranslateAnimation anim = new TranslateAnimation(0, 0, 0, -300);
                    anim.setDuration(2000); //2000
                    anim.setFillAfter(true);
                    //ViewGroup.LayoutParams params = iv.getLayoutParams();
                    //params.width = 100;
                    //iv.setLayoutParams(params);
                    iv.startAnimation(anim);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            rl.setTranslationY(-(300));
                            startAnimation.setInterpolator(new AccelerateInterpolator());
                            View root = findViewById(R.id.rl1);
                            ObjectAnimator fadeIn = ObjectAnimator.ofFloat(root, "alpha", 0f, 1f);
                            fadeIn.setDuration(1500);
                            fadeIn.start();
                            // rl.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }

                    });


                }
            }, 2000);
        } else {
            iv.setTranslationY(-(300));
            rl.setTranslationY(-(300));
            rl.setAlpha(1f);
        }
    }

    SweetAlertDialog pDialog;
    String userName, password;

    public void openDrawer(View view) {
        if (MySingleton.getInstance().isConnected(this)) {
            pDialog = new SweetAlertDialog(_Splash.this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#cb3439"));
            pDialog.setTitleText("Logging In - Please Wait");
            pDialog.setCancelable(false);
            pDialog.show();

            TextView uname = (TextView) findViewById(R.id.email);
            TextView pass = (TextView) findViewById(R.id.password);

            if (toggleSwitch.getCheckedTogglePosition() == 0)
                userName = uname.getText().toString() + "@webdoc.com.pk";
            else
                userName = uname.getText().toString() + "@velocitycallcenter.com";

            password = pass.getText() + "";

            GetProfile asyncTask = new GetProfile(this, new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    if (result == null) {
                        pDialog.dismiss();
                        new SweetAlertDialog(_Splash.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                        "If the problem persists, please contact us at support@webdoc.com.pk")
                                .show();
                        return;
                    } else if (result.equals("")) {
                    } else {
                        try {
                            JSONArray arr = ((JSONObject) result).getJSONArray("loginResult");
                            JSONObject obj = arr.getJSONObject(0);

                            if (obj.getString("Response").equals("Success")) {
                                if (!getSinchServiceInterface().isStarted()) {
                                    getSinchServiceInterface().startClient(userName);
                                    onStarted();
                                }
                            } else if (obj.getString("Response").equals("Fail")) {
                                pDialog.dismiss();
                                Snackbar.make(findViewById(android.R.id.content), "Invalid Email or Password", Snackbar.LENGTH_LONG)
                                        .setActionTextColor(Color.RED)
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, "login/" + userName + "/" + password + "__" + "0");
            asyncTask.execute();

        }
    }

    public void saveInfo(String name, String pass) {
        SharedPreferences sharedpref = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        sharedpref.edit().clear().commit();
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString("UserName", name);
        editor.putString("Password", pass);
        editor.apply();
    }

    private void checkInfo() {
        SharedPreferences sharedpref = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        String uname = sharedpref.getString("UserName", "0");
        String pass = sharedpref.getString("Password", "0");

        if (uname != "0" && pass != "0") {
            MySingleton.getInstance().setUserId(uname);
            startActivity(new Intent(this, _MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (true) {

                    startEverything();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(_MainActivity.this, "Permission denied to read Record Audio", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onStartFailed(SinchError error) {
        pDialog.dismiss();
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {
        MySingleton.getInstance().setUserId(userName);
        saveInfo(userName, password);
        startActivity(new Intent(getApplicationContext(), _MainActivity.class));
        pDialog.dismiss();
        finish();
    }

    public void forgotPassword(View view) {
        startActivity(new Intent(this, _ForgotActivity.class));
    }
}
