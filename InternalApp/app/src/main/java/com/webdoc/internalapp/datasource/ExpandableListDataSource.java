package com.webdoc.internalapp.datasource;

import android.content.Context;

import com.webdoc.internalapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by msahakyan on 22/10/15.
 */
public class ExpandableListDataSource {

    /**
     * Returns fake data of films
     *
     * @param context
     * @return
     */
    public static Map<String, List<String>> getData(Context context) {
        Map<String, List<String>> expandableListData = new LinkedHashMap<>();

        List<String> Main = Arrays.asList(context.getResources().getStringArray(R.array.Main));

        List<String> Kitchen_Buzzer = Arrays.asList(context.getResources().getStringArray(R.array.Kitchen_Buzzer));
        List<String> General_Profile = Arrays.asList(context.getResources().getStringArray(R.array.General_Profile));
        List<String> My_Health = Arrays.asList(context.getResources().getStringArray(R.array.My_Health));
        List<String> Insurance_Products = Arrays.asList(context.getResources().getStringArray(R.array.Insurance_Products));
        // List<String> Corporate_Panels = Arrays.asList(context.getResources().getStringArray(R.array.Corporate_Panels));
        List<String> Logout = Arrays.asList(context.getResources().getStringArray(R.array.Logout));

        expandableListData.put(Main.get(0), Kitchen_Buzzer);
        expandableListData.put(Main.get(1), General_Profile);
        expandableListData.put(Main.get(2), My_Health);
        expandableListData.put(Main.get(3), Insurance_Products);
        expandableListData.put(Main.get(4), Logout);

        return expandableListData;
    }

    public static ArrayList getIcons(Context context) {

        ArrayList<Integer> arr = new ArrayList<Integer>();

        arr.add(R.drawable.__buzzer);
        arr.add(R.drawable.__gprofile);//_gprofile
        arr.add(R.drawable.__chatbox);//hhealth
        arr.add(R.drawable.__kitchen);//_insurance
        //   arr.add(R.drawable.__corporate);//ccorporate
        arr.add(R.drawable.__logout);//logout

        return arr;
    }
}
