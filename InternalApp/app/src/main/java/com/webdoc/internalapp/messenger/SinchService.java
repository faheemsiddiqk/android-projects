package com.webdoc.internalapp.messenger;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.MissingGCMException;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.messaging.Message;
import com.sinch.android.rtc.messaging.MessageClient;
import com.sinch.android.rtc.messaging.MessageClientListener;
import com.sinch.android.rtc.messaging.MessageDeliveryInfo;
import com.sinch.android.rtc.messaging.MessageFailureInfo;
import com.sinch.android.rtc.messaging.WritableMessage;
import com.webdoc.internalapp.DBHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import br.com.goncalves.pugnotification.notification.PugNotification;

public class SinchService extends Service {

    /*private static final String APP_KEY = "89122446-2044-41ca-b7de-b5fd9cc22708";
    private static final String APP_SECRET = "60jzHnq6yEWAcZytAJSRtg==";
    private static final String ENVIRONMENT = "sandbox.sinch.com";*/

    private static final String APP_KEY = "0799ac52-aa22-4c6e-85a7-53b1c9a041ab";
    private static final String APP_SECRET = "A1j8Zp811EqX/WEtBDB7JA==";
    private static final String ENVIRONMENT = "clientapi.sinch.com";

    private static final String TAG = SinchService.class.getSimpleName();


    private PersistedSettings mSettings;
    private final SinchServiceInterface mServiceInterface = new SinchServiceInterface();
    private SinchClient mSinchClient = null;
    private String mUserId;
    private StartFailedListener mListener;

    FirebaseDatabase database;
    DatabaseReference myRef;

    DBHelper DB;

    void dbMethod(String name) {
        //Toast.makeText(this, result.getMessageResult().toString(), Toast.LENGTH_SHORT).show();

        if (DB.getData(name)) {
            DB.messagesRead(name, "0");
        } else {
            DB.insertPatient(name);
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSettings = new PersistedSettings(getApplicationContext());

        registerReceiver(myReceiver, new IntentFilter("my.awersome.string.name"));
        DB = new DBHelper(this);
    }

    private final BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            PugNotification.with(getApplicationContext()).cancel(intent.getIntExtra("id", 0));
            DB.deleteNotification_(intent.getIntExtra("id", 0) + "");
        }
    };

    @Override
    public void onDestroy() {
        if (mSinchClient != null && mSinchClient.isStarted()) {
            mSinchClient.terminate();
        }
        super.onDestroy();
    }

    private void start(String userName) {
        if (mSinchClient == null) {
            mSinchClient = Sinch.getSinchClientBuilder().context(getApplicationContext()).userId(userName)
                    .applicationKey(APP_KEY)
                    .applicationSecret(APP_SECRET)
                    .environmentHost(ENVIRONMENT).build();

            mSinchClient.setSupportMessaging(true);

            try {
                mSinchClient.setSupportManagedPush(true);
            } catch (MissingGCMException e) {
                final String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
            mSinchClient.startListeningOnActiveConnection();
            mSinchClient.checkManifest();

            mSinchClient.addSinchClientListener(new MySinchClientListener());
            mSinchClient.getMessageClient().addMessageClientListener(new MessageClientListener() {
                @Override
                public void onIncomingMessage(MessageClient messageClient, Message message) {
                    String id = message.getMessageId();
                    String body = message.getTextBody();

                    String recipient = message.getRecipientIds() + "";
                    recipient = recipient.replace("[", "").replace("]", "");

                    String sender = message.getSenderId();
                    String timestamp = message.getTimestamp() + "";

                    String direction = MessageAdapter.DIRECTION_INCOMING + "";

                    dbMethod(sender);
                    DB.insertMessage(id, body, recipient, sender, timestamp, direction);
                }

                @Override
                public void onMessageSent(MessageClient messageClient, Message message, String s) {
                    String id = message.getMessageId();
                    String body = message.getTextBody();

                    String recipient = message.getRecipientIds() + "";
                    recipient = recipient.replace("[", "").replace("]", "");

                    String sender = message.getSenderId();
                    String timestamp = message.getTimestamp() + "";

                    String direction = MessageAdapter.DIRECTION_OUTGOING + "";


                    DB.insertMessage(id, body, recipient, sender, timestamp, direction);
                }

                @Override
                public void onMessageFailed(MessageClient messageClient, Message message, MessageFailureInfo messageFailureInfo) {

                }

                @Override
                public void onMessageDelivered(MessageClient messageClient, MessageDeliveryInfo messageDeliveryInfo) {

                }

                @Override
                public void onShouldSendPushData(MessageClient messageClient, Message message, List<PushPair> list) {

                }
            });

            mSinchClient.start();
        }
    }

    private void stop() {
        if (mSinchClient != null) {
            mSinchClient.terminateGracefully();
            mSinchClient = null;
        }
    }

    private boolean isStarted() {
        return (mSinchClient != null && mSinchClient.isStarted());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mServiceInterface;
    }

    public class SinchServiceInterface extends Binder {

        public String getUserId() {
            return mUserId;
        }

        SimpleDateFormat mFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.US);

        public void sendFirebase(String recipient, String body, String messageId) {

            Map notification = new HashMap<>();
            notification.put("id", messageId);
            notification.put("sender", mUserId);
            notification.put("receiver", recipient);
            notification.put("message", body);

            String timestamp = null;
            try {
                Date a = mFormatter.parse(Calendar.getInstance().getTime().toString());
                timestamp = a + "";
            } catch (ParseException e) {
                e.printStackTrace();
            }
            notification.put("timestamp", timestamp);
            notification.put("shown", "0");

            myRef.push().setValue(notification);
        }

        public void sendSystemFirebase(String body, String messageId) {

            Map notification = new HashMap<>();
            notification.put("id", messageId);
            notification.put("sender", "system@webdoc.com.pk");
            notification.put("receiver", "");
            notification.put("message", mUserId + " : " + body);

            String timestamp = null;
            try {
                Date a = mFormatter.parse(Calendar.getInstance().getTime().toString());
                timestamp = a + "";
            } catch (ParseException e) {
                e.printStackTrace();
            }
            notification.put("timestamp", timestamp);
            notification.put("shown", "0");

            myRef.push().setValue(notification);
        }

        public boolean isStarted() {
            return SinchService.this.isStarted();
        }

        public void startClient(String userName) {
            mUserId = userName;
            mSettings.setUsername(userName);
            //start(userName);

            // Write a message to the database
            database = FirebaseDatabase.getInstance();
            myRef = database.getReference("WebDocInternal");
        }

        public void stopClient() {
            stop();
        }

        public void setStartListener(StartFailedListener listener) {
            mListener = listener;
        }

        public void sendMessage(String recipientUserId, String textBody) {
            SinchService.this.sendMessage(recipientUserId, textBody);
        }

        public void addMessageClientListener(MessageClientListener listener) {
            SinchService.this.addMessageClientListener(listener);
        }

        public void removeMessageClientListener(MessageClientListener listener) {
            SinchService.this.removeMessageClientListener(listener);
        }

        public NotificationResult relayRemotePushNotificationPayload(Intent intent) {
            if (mSinchClient == null && !mSettings.getUsername().isEmpty()) {
                start(mSettings.getUsername());
            } else if (mSinchClient == null && mSettings.getUsername().isEmpty()) {
                Log.e(TAG, "Can't start a SinchClient as no username is available, unable to relay push.");
                return null;
            }
            return mSinchClient.relayRemotePushNotificationPayload(intent);
        }
    }

    public interface StartFailedListener {

        void onStartFailed(SinchError error);

        void onStarted();
    }

    private class MySinchClientListener implements SinchClientListener {

        @Override
        public void onClientFailed(SinchClient client, SinchError error) {
            if (mListener != null) {
                mListener.onStartFailed(error);
            }
            mSinchClient.terminate();
            mSinchClient = null;
        }

        @Override
        public void onClientStarted(SinchClient client) {
            Log.d(TAG, "SinchClient started");
            if (mListener != null) {
                mListener.onStarted();
            }
        }

        @Override
        public void onClientStopped(SinchClient client) {
            Log.d(TAG, "SinchClient stopped");
        }

        @Override
        public void onLogMessage(int level, String area, String message) {
            switch (level) {
                case Log.DEBUG:
                    Log.d(area, message);
                    break;
                case Log.ERROR:
                    Log.e(area, message);
                    break;
                case Log.INFO:
                    Log.i(area, message);
                    break;
                case Log.VERBOSE:
                    Log.v(area, message);
                    break;
                case Log.WARN:
                    Log.w(area, message);
                    break;
            }
        }

        @Override
        public void onRegistrationCredentialsRequired(SinchClient client,
                                                      ClientRegistration clientRegistration) {
        }
    }

    public void sendMessage(String recipientUserId, String textBody) {
        if (isStarted()) {
            WritableMessage message = new WritableMessage(recipientUserId, textBody);
            mSinchClient.getMessageClient().send(message);
        }
    }

    public void addMessageClientListener(MessageClientListener listener) {
        if (mSinchClient != null) {
            mSinchClient.getMessageClient().addMessageClientListener(listener);
        }
    }

    public void removeMessageClientListener(MessageClientListener listener) {
        if (mSinchClient != null) {
            mSinchClient.getMessageClient().removeMessageClientListener(listener);
        }
    }


    private class PersistedSettings {

        private SharedPreferences mStore;

        private static final String PREF_KEY = "Sinch";

        public PersistedSettings(Context context) {
            mStore = context.getSharedPreferences(PREF_KEY, MODE_PRIVATE);
        }

        public String getUsername() {
            return mStore.getString("Username", "");
        }

        public void setUsername(String username) {
            SharedPreferences.Editor editor = mStore.edit();
            editor.putString("Username", username);
            editor.commit();
        }
    }
}
