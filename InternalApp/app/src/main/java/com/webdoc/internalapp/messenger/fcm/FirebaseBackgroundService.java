package com.webdoc.internalapp.messenger.fcm;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.sinch.android.rtc.messaging.Message;
import com.webdoc.internalapp.DBHelper;
import com.webdoc.internalapp.MySingleton;
import com.webdoc.internalapp.R;
import com.webdoc.internalapp.messenger.AudioPlayer;
import com.webdoc.internalapp.messenger.MessageAdapter;
import com.webdoc.internalapp.messenger.Messenger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import br.com.goncalves.pugnotification.notification.PugNotification;

/**
 * Created by faheem on 04/12/2017.
 */

public class FirebaseBackgroundService extends Service {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("WebDocInternal");
    DBHelper DB;

    Intent intent;
    ActivityManager am;
    List<ActivityManager.RunningTaskInfo> taskInfo;
    String classname;
    Boolean _b_;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        DB = new DBHelper(getApplicationContext());

        intent = new Intent(getApplicationContext(), Messenger.class);

        am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        // Read from the database
        Query query = myRef.orderByChild("receiver").equalTo(userName());

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    DataSnapshot child = dataSnapshot;

                    String key = child.getKey();

                    String id = (String) child.child("id").getValue();
                    String message = (String) child.child("message").getValue();
                    String sender = (String) child.child("sender").getValue();
                    String receiver = (String) child.child("receiver").getValue();
                    String shown = (String) child.child("shown").getValue();
                    String timestamp = (String) child.child("timestamp").getValue();
                    String direction = MessageAdapter.DIRECTION_INCOMING + "";

                    if (shown == null)
                        return;

                    if (shown.equals("0")) {
                        DB.insertMessage(id, message, receiver, sender, timestamp, direction);
                        Map notification = new HashMap<>();
                        notification.put("shown", "1");
                        myRef.child(key).updateChildren(notification);

                        Message m = newMessage(id, message, receiver, sender, timestamp);
                        showNofication(sender, message);
                        try {
                            Messenger.updateList(m);
                        } catch (NullPointerException e) {
                        }

                    }

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d("", "changed");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("", "removed");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d("", "moved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("", "cancelled");
            }
        });

        // Read from the database
        Query _query_ = myRef.orderByChild("sender").equalTo("system@webdoc.com.pk");

        _query_.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    DataSnapshot child = dataSnapshot;

                    String key = child.getKey();

                    String id = (String) child.child("id").getValue();
                    String message = (String) child.child("message").getValue();
                    String sender = (String) child.child("sender").getValue();
                    String receiver = userName();
                    String shown = (String) child.child("shown").getValue();
                    String timestamp = (String) child.child("timestamp").getValue();
                    String direction = MessageAdapter.DIRECTION_INCOMING + "";

                    if (message.toLowerCase().contains(userName().toLowerCase().split("@")[0]))
                        return;

                    if (shown == null)
                        return;

                    if (shown.equals("0")) {
                        DB.insertMessage(id, message, receiver, sender, timestamp, direction);
                        Map notification = new HashMap<>();
                        notification.put("shown", "1");
                        myRef.child(key).updateChildren(notification);

                        Message m = newMessage(id, message, receiver, sender, timestamp);
                        showNofication(sender, message);
                        try {
                            Messenger.updateList(m);
                        } catch (NullPointerException e) {
                        }

                    }

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d("", "changed");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("", "removed");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d("", "moved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("", "cancelled");
            }
        });

    }

    void showNofication(String sender, String message) {

        taskInfo = am.getRunningTasks(1);
        classname = taskInfo.get(0).topActivity.getClassName();
        _b_ = classname.equals("com.webdoc.internalapp.messenger.Messenger");
        if (!_b_ || !MySingleton.getInstance().checkRecipient(sender)) {
            int count = 0;
            String a = sender;

            DB.insertNotification_("", a, "Message");
            count = DB.getNotification_(a, "Message");

            String _name_ = sender;

            intent.putExtra("Name", _name_);
            intent.putExtra("UserName", sender);
            intent.putExtra("Status", "0");
            intent.putExtra("MyName", userName());
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pi = PendingIntent.getActivity(this, 10, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent stopIntent = new Intent("my.awersome.string.name");
            stopIntent.putExtra("id", count);
            PendingIntent stopPi = PendingIntent.getBroadcast(getApplicationContext(), 4, stopIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            PugNotification.with(getApplicationContext())
                    .load()
                    .identifier(count)
                    .title(sender)
                    .message(message)
                    .bigTextStyle(message)
                    .smallIcon(R.mipmap.ic_launcher)
                    .largeIcon(R.mipmap.ic_launcher)
                    .priority(Notification.PRIORITY_HIGH)
                    .autoCancel(true)
                    .flags(Notification.DEFAULT_VIBRATE)
                    .vibrate(new long[]{100})
                    .dismiss(stopPi)
                    .click(pi)
                    .simple()
                    .build();

            new AudioPlayer(getApplicationContext()).playMsg();

        }
    }

    private Message newMessage(final String id, final String body, final String receipient, final String sender, final String timeStamp) {
        Date finalD = null;
        SimpleDateFormat mFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.US);
        try {
            Date d1 = (Date) mFormatter.parse(timeStamp);
            finalD = d1;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final Date finalD1 = finalD;
        Message m = new Message() {
            @Override
            public String getMessageId() {
                return id;
            }

            @Override
            public Map<String, String> getHeaders() {
                return null;
            }

            @Override
            public String getTextBody() {
                return body;
            }

            @Override
            public List<String> getRecipientIds() {
                return new ArrayList<>(Arrays.asList(receipient));
            }

            @Override
            public String getSenderId() {
                return sender;
            }

            @Override
            public Date getTimestamp() {
                return finalD1;
            }
        };
        return m;
    }

    private String userName() {
        SharedPreferences sharedpref = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        String uname = sharedpref.getString("UserName", "0");
        String pass = sharedpref.getString("Password", "0");

        return uname;
    }

}
