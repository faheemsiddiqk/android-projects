package com.webdoc.internalapp.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.caverock.androidsvg.SVG;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.webdoc.internalapp.AsyncResponse;
import com.webdoc.internalapp.GetNetwork;
import com.webdoc.internalapp.MySingleton;
import com.webdoc.internalapp.R;
import com.webdoc.internalapp.dialog.QustomDialogBuilder;
import com.webdoc.internalapp.messenger.BaseActivityFragment;
import com.webdoc.internalapp.util.CircleAnimationUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by faheem on 04/07/2017.
 */

public class _3_FragmentKitchen extends BaseActivityFragment {

    int width, height;

    private static final String KEY_MOVIE_TITLE = "key_title";

    ArrayList<ListData> main;
    static ArrayList<ListData> main_;
    ListView lv;

    ArrayList<Trolley> final_;

    ArrayList<String> TrolleyDataNew;
    ArrayList<String> TrolleyDataOld;

    ImageButton fab;
    FloatingActionMenu menuGreen;
    static FloatingActionMenu menuGreen_;

    private List<FloatingActionMenu> menus = new ArrayList<>();
    private Handler mUiHandler = new Handler();


    public _3_FragmentKitchen() {
    }

    static _3_FragmentKitchen fragment = new _3_FragmentKitchen();

    public static _3_FragmentKitchen newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            fragment.setArguments(args);
        } catch (IllegalStateException e) {
            fragment = new _3_FragmentKitchen();
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.__fragment_3_fragmentkitchen, container, false);
    }

    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        height = MySingleton.getInstance().getHeight();
        width = MySingleton.getInstance().getWidth();

        main = new ArrayList();
        TrolleyDataNew = new ArrayList();
        TrolleyDataOld = new ArrayList();
        final_ = new ArrayList<Trolley>();

        /*main.add(new ListData("1_1", "", "Tea", "", "Coffee"));
        main.add(new ListData("1", "", "Book Left", "", "Book Right"));
        main.add(new ListData("2", "", "Green Tea", "", ""));
        main.add(new ListData("3", "", "", "", ""));*/

        main_ = main;

        lv = (ListView) view.findViewById(R.id.listView);
        //lv.setAdapter(new SampleAdapter());
        fab = (ImageButton) view.findViewById(R.id.fabButton);

        menuGreen = (FloatingActionMenu) view.findViewById(R.id.menu_green);

        ((FloatingActionButton) view.findViewById(R.id.fabconfirm)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmOrder(view);
            }
        });
        ((FloatingActionButton) view.findViewById(R.id.fabclear)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearOrder(view);
            }
        });

        try {
            JSONArray arr = MySingleton.getInstance().getArray(2);//((JSONObject) result).getJSONArray("KitchenItemsResult");
            JSONObject obj;
            for (int i = 0; i < arr.length(); i++) {
                obj = arr.getJSONObject(i);
                main.add(new ListData(obj.getString("LId") + "_" + obj.getString("RId"), obj.getString("LImageLink"), obj.getString("LName"), obj.getString("RImageLink"), obj.getString("RName")));
            }
            if (main.size() < 4) {
                for (int i = main.size(); i < 4; i++)
                    main.add(new ListData("EMPTY", "", "", "", ""));
            }
            lv.setAdapter(new SampleAdapter());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        menuGreen.bringToFront();
        menuGreen.invalidate();

        menuGreen.hideMenuButton(false);

        /*Utility.setListViewHeightBasedOnChildren(lv);
        Boolean a = Utility.setListViewHeightBasedOnItems(lv);*/

        lv.requestFocus();

        //fab.bringToFront();
        //fab.invalidate();

        //fab.hideMenuButton(false);

        menuGreen_ = menuGreen;

        menus.add(menuGreen);

        createCustomAnimation();
    }

    private void createCustomAnimation() {
        AnimatorSet set = new AnimatorSet();

        ObjectAnimator scaleOutX = ObjectAnimator.ofFloat(menuGreen.getMenuIconView(), "scaleX", 1.0f, 0.2f);
        ObjectAnimator scaleOutY = ObjectAnimator.ofFloat(menuGreen.getMenuIconView(), "scaleY", 1.0f, 0.2f);

        ObjectAnimator scaleInX = ObjectAnimator.ofFloat(menuGreen.getMenuIconView(), "scaleX", 0.2f, 1.0f);
        ObjectAnimator scaleInY = ObjectAnimator.ofFloat(menuGreen.getMenuIconView(), "scaleY", 0.2f, 1.0f);

        scaleOutX.setDuration(50);
        scaleOutY.setDuration(50);

        scaleInX.setDuration(150);
        scaleInY.setDuration(150);

        scaleInX.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {

                if (menuGreen.isOpened()) {
                    if (TrolleyDataOld != TrolleyDataNew) {
                        final_.clear();
                        for (int i = 0; i < TrolleyDataNew.size(); i++) {
                            String a = TrolleyDataNew.get(i);

                            boolean abc = _check_(a);

                            if (!abc) {
                                final_.add(new Trolley(a));
                            } else {
                                final_.get(_indexof_(a)).Count++;
                            }
                        }
                        TrolleyDataOld.clear();
                        for (int i = 0; i < TrolleyDataNew.size(); i++) {
                            TrolleyDataOld.add(TrolleyDataNew.get(i));
                        }

                    }
                }
                int d;
                if (TrolleyDataNew.size() == 0)
                    d = R.drawable.__emptyc;
                else
                    d = R.drawable.__fullc;


                menuGreen.getMenuIconView().setImageResource(menuGreen.isOpened()
                        ? R.drawable.__gotrolley : d);
            }
        });

        set.play(scaleOutX).with(scaleOutY);
        set.play(scaleInX).with(scaleInY).after(scaleOutX);
        set.setInterpolator(new OvershootInterpolator(2));

        menuGreen.setIconToggleAnimatorSet(set);
    }

    public void confirmOrder(View view) {
        String a = "";
        for (int i = 0; i < final_.size(); i++) {
            a += final_.get(i).Count + " of " + final_.get(i).Name + "\n";
        }
        a = a.trim();

        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        pDialog.setTitleText("Confirm Your Order !");
        pDialog.setContentText(a);
        pDialog.setCustomImage(R.drawable.logowebdoc);
        pDialog.setConfirmText("Yes");
        final String finalA = a;
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();

                GetNetwork async = new GetNetwork(getActivity(), new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        Boolean a = (Boolean) result;
                        if (!a) {
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("No Network")
                                    .show();
                            return;
                        } else {
                            QustomDialogBuilder qustomDialogBuilder = new QustomDialogBuilder(getActivity()).
                                    setTitle("Room Picker").
                                    setTitleColor("#CB3439").
                                    setDividerColor("#CB3439").
                                    setMessage("What is your location ?").
                                    setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

                            qustomDialogBuilder.setOrder(finalA);
                            qustomDialogBuilder.setSinchInterface(getSinchServiceInterface());
                            android.app.AlertDialog ad = qustomDialogBuilder.show();
                            qustomDialogBuilder.setDialog(ad);


                            final_.clear();
                            TrolleyDataNew.clear();
                            menuGreen.hideMenuButton(false);
                        }
                    }
                }, "");
                async.execute();

            }
        });
        pDialog.setCancelText("No");
        pDialog.showCancelButton(true);
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        });
        pDialog.show();
        /**/
        menuGreen.toggle(true);
    }

    public void clearOrder(View view) {
        final_.clear();
        TrolleyDataNew.clear();
        TrolleyDataOld.clear();
        menuGreen.hideMenuButton(false);
        menuGreen.toggle(true);

    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
               /* if (position == main.size() - 1)
                    convertView = View.inflate(getContext(), R.layout.___list_item_kitchenlast, null);
                else*/
                convertView = View.inflate(getContext(), R.layout.___list_item_kitchen, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);

            if (item.Name.equals("EMPTY")) {
                holder.ProgressBarL.setVisibility(View.GONE);
                holder.ProgressBarR.setVisibility(View.GONE);
                return convertView;
            }

            if (!item.NameL.equals("")) {
                holder.NameL.setText(item.NameL);
                holder.imageViewL.setContentDescription(position + "");

                holder.ProgressBarL.setVisibility(View.VISIBLE);


                if (linkCheck(item.DrawableL)) {

                    Glide.with(getActivity())
                            .load(item.DrawableL)
                            .override(width, height)
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    holder.ProgressBarL.setVisibility(View.GONE);
                                    holder.NameL.setVisibility(View.VISIBLE);
                                    return false;
                                }
                            })
                            .into(holder.imageViewL);

                    holder.imageViewL.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (TrolleyDataNew.size() == 0)
                                for (final FloatingActionMenu menu : menus) {
                                    mUiHandler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            menu.showMenuButton(true);
                                        }
                                    }, 750);
                                }

                            makeFlyAnimation((ImageView) view, Integer.parseInt(view.getContentDescription() + ""), "Left");
                            ListData _d_ = main_.get(Integer.parseInt(view.getContentDescription() + ""));
                            TrolleyDataNew.add(_d_.NameL);
                        }
                    });
                }
            }

            if (!item.NameR.equals("")) {
                holder.NameR.setText(item.NameR);
                holder.imageViewR.setContentDescription(position + "");

                holder.ProgressBarR.setVisibility(View.VISIBLE);

                if (linkCheck(item.DrawableR)) {
                    Glide.with(getActivity())
                            .load(item.DrawableR)
                            .override(width, height)
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    holder.ProgressBarR.setVisibility(View.GONE);
                                    holder.NameR.setVisibility(View.VISIBLE);
                                    return false;
                                }
                            })
                            .into(holder.imageViewR);

                    holder.imageViewR.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (TrolleyDataNew.size() == 0)
                                for (final FloatingActionMenu menu : menus) {
                                    mUiHandler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            menu.showMenuButton(true);
                                        }
                                    }, 750);
                                }

                            makeFlyAnimation((ImageView) view, Integer.parseInt(view.getContentDescription() + ""), "Right");
                            ListData _d_ = main_.get(Integer.parseInt(view.getContentDescription() + ""));
                            TrolleyDataNew.add(_d_.NameR);
                        }
                    });
                }
            }

            return convertView;

        }
    }

    private static class ViewHolder {
        private View view;

        private TextView NameL;
        private ImageView imageViewL;
        private ProgressBar ProgressBarL;

        private TextView NameR;
        private ImageView imageViewR;
        private ProgressBar ProgressBarR;

        private ViewHolder(View view) {
            this.view = view;
            NameL = (TextView) view.findViewById(R.id.NameL);
            ProgressBarL = (ProgressBar) view.findViewById(R.id.progressBarL);
            imageViewL = (ImageView) view.findViewById(R.id.imageLeft);

            NameR = (TextView) view.findViewById(R.id.NameR);
            ProgressBarR = (ProgressBar) view.findViewById(R.id.progressBarR);
            imageViewR = (ImageView) view.findViewById(R.id.imageRight);
        }
    }

    private class ListData {
        private String Name;
        private String DrawableL;
        private String NameL;
        private String DrawableR;
        private String NameR;

        public ListData(String n, String dl, String nl, String dr, String nr) {
            Name = n;

            if (!nl.equals("null")) {
                NameL = nl;
                DrawableL = dl;
            } else {
                NameL = "";
                DrawableL = "";
            }

            if (!nr.equals("null")) {
                NameR = nr;
                DrawableR = dr;
            } else {
                NameR = "";
                DrawableR = "";
            }
        }
    }

    private class Trolley {
        private String Name;
        private int Count = 1;

        public Trolley(String n) {
            Name = n;
        }
    }

    private void makeFlyAnimation(ImageView targetView, int index, String direction) {

        new CircleAnimationUtil().attachActivity(getActivity()).setTargetView(targetView, index, direction).setMoveDuration(1000).setDestView(fab).setAnimationListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //Toast.makeText(MainActivity.this, "Continue Shopping...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).startAnimation();
    }

    public static void fullCart(ImageView m, int i, String d) {


        AnimatorSet set = new AnimatorSet();

        ObjectAnimator scaleOutX = ObjectAnimator.ofFloat(menuGreen_.getMenuIconView(), "scaleX", 1.0f, 0.2f);
        ObjectAnimator scaleOutY = ObjectAnimator.ofFloat(menuGreen_.getMenuIconView(), "scaleY", 1.0f, 0.2f);

        ObjectAnimator scaleInX = ObjectAnimator.ofFloat(menuGreen_.getMenuIconView(), "scaleX", 0.2f, 1.0f);
        ObjectAnimator scaleInY = ObjectAnimator.ofFloat(menuGreen_.getMenuIconView(), "scaleY", 0.2f, 1.0f);

        scaleOutX.setDuration(750);
        scaleOutY.setDuration(750);

        scaleInX.setDuration(750);
        scaleInY.setDuration(750);

        set.play(scaleOutX).with(scaleOutY);
        set.play(scaleInX).with(scaleInY).after(scaleOutX);
        set.setInterpolator(new OvershootInterpolator(2));
        set.start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                menuGreen_.getMenuIconView().setImageResource(R.drawable.__fullc);
            }
        }, 375);

    }

    public boolean _check_(String a) {
        for (int i = 0; i < final_.size(); i++) {
            if (final_.get(i).Name.equals(a))
                return true;
        }
        return false;
    }

    public int _indexof_(String a) {
        for (int i = 0; i < final_.size(); i++) {
            if (final_.get(i).Name.equals(a))
                return i;
        }
        return 0;
    }


    public boolean linkCheck(String contentUri) {
        URLConnection connection = null;
        try {
            connection = new URL(contentUri).openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

}
