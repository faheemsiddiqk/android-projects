package com.webdoc.internalapp.dialog;

/**
 * Created by faheem on 21/11/2017.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.webdoc.internalapp.R;
import com.webdoc.internalapp.messenger.SinchService;

import java.util.UUID;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class QustomDialogBuilder extends AlertDialog.Builder {

    android.app.AlertDialog ad;
    private SinchService.SinchServiceInterface mSinchServiceInterface;
    private String order = "";
    /**
     * The custom_body layout
     */
    private View mDialogView;

    /**
     * optional dialog title layout
     */
    private TextView mTitle;
    /**
     * optional alert dialog image
     */
    private ImageView mIcon;
    /**
     * optional message displayed below title if title exists
     */
    private TextView mMessage;
    /**
     * The colored holo divider. You can set its color with the setDividerColor method
     */
    private View mDivider;

    private ListView lv;
    private String[] titles = {"meetingRoom", "gmRoom", "conferenceRoom", "cpoRoom", "diningRoom", "doctorsRoom", "velocity", "webdoc"};

    public QustomDialogBuilder(Context context) {
        super(context);

        mDialogView = View.inflate(context, R.layout.qustom_dialog_layout, null);
        setView(mDialogView);

        mTitle = (TextView) mDialogView.findViewById(R.id.alertTitle);
        mMessage = (TextView) mDialogView.findViewById(R.id.message);
        mIcon = (ImageView) mDialogView.findViewById(R.id.icon);
        mDivider = mDialogView.findViewById(R.id.titleDivider);
        lv = (ListView) mDialogView.findViewById(R.id.listView);
        lv.setAdapter(new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, titles));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(), titles[i], Toast.LENGTH_LONG).show();
                //mSinchServiceInterface.sendMessage("kitchen@webdoc.com.pk", getOrder() + "###" + titles[i]);
                mSinchServiceInterface.sendFirebase("kitchen123@webdoc.com.pk", getOrder() + "###" + titles[i], UUID.randomUUID().toString());

                ad.dismiss();

                if (getOrder().equals("***")) {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Please Wait !")
                            .setContentText("Someone will be with you shortly")
                            .show();
                } else {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Order Placed !")
                            .setContentText("Your order will be delivered shortly")
                            .show();
                }
            }
        });
    }

    /**
     * Use this method to color the divider between the title and content.
     * Will not display if no title is set.
     *
     * @param colorString for passing "#ffffff"
     */
    public QustomDialogBuilder setDividerColor(String colorString) {
        mDivider.setBackgroundColor(Color.parseColor(colorString));
        return this;
    }

    @Override
    public QustomDialogBuilder setTitle(CharSequence text) {
        mTitle.setText(text);
        return this;
    }

    public QustomDialogBuilder setTitleColor(String colorString) {
        mTitle.setTextColor(Color.parseColor(colorString));
        return this;
    }

    @Override
    public QustomDialogBuilder setMessage(int textResId) {
        mMessage.setText(textResId);
        return this;
    }

    @Override
    public QustomDialogBuilder setMessage(CharSequence text) {
        mMessage.setText(text);
        return this;
    }

    @Override
    public QustomDialogBuilder setIcon(int drawableResId) {
        mIcon.setImageResource(drawableResId);
        return this;
    }

    @Override
    public QustomDialogBuilder setIcon(Drawable icon) {
        mIcon.setImageDrawable(icon);
        return this;
    }

    @Override
    public AlertDialog show() {
        if (mTitle.getText().equals(""))
            mDialogView.findViewById(R.id.topPanel).setVisibility(View.GONE);
        return super.show();
    }

    public void setOrder(String o) {
        order = o;
    }

    public String getOrder() {
        return order;
    }

    public void setSinchInterface(SinchService.SinchServiceInterface s) {
        mSinchServiceInterface = s;
    }

    public void setDialog(AlertDialog a) {
        ad = a;
    }
}