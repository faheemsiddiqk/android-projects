package com.sidra.smarthome;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A fragment that launches other parts of the demo application.
 */
public class _Manual extends Fragment {

    TabLayout tabLayout;
    ViewPager viewPager;
    _Manual_Interaction mif;
    _Manual_Speech msf;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.__fragment_manual, container,
                false);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        tabLayout = (TabLayout) view.findViewById(R.id.manual_tabs);
        viewPager = (ViewPager) view.findViewById(R.id.manual_container);

        mif = new _Manual_Interaction();
        msf = new _Manual_Speech();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.__interaction);
        tabLayout.getTabAt(1).setIcon(R.drawable.__speech);

        viewPager.setOffscreenPageLimit(0);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPageAdapter Adapter = new ViewPageAdapter(getChildFragmentManager());
        Adapter.addFragments(mif);
        Adapter.addFragments(msf);
        viewPager.setAdapter(Adapter);
    }

    public class ViewPageAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> fragments = new ArrayList<>();

        public ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragments(Fragment fragment) {
            this.fragments.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

    }
}
