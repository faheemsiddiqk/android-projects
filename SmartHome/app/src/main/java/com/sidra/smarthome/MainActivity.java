package com.sidra.smarthome;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    SampleAdapter sd;
    ArrayList<ListData> mDataList;
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private TextDrawable.IBuilder mDrawableBuilder;

    private RequestQueue requestQueue;
    private static final String URL = "http://websystemsfahim.webddocsystems.com/Sidra/fetchAccessLogEntries.php";
    private StringRequest request;

    ListView lv;

    TabLayout tabLayout;
    ViewPager viewPager;

    _Manual mf;
    _Automatic af;

    String address = null;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    //SPP UUID. Look for it
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sd = new SampleAdapter();
        requestQueue = Volley.newRequestQueue(this);
        fetchAccessLog();
        mDrawableBuilder = TextDrawable.builder()
                .beginConfig()
                .withBorder(4)
                .endConfig()
                .round();
        lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(sd);

        Intent newint = getIntent();
        address = newint.getStringExtra(DeviceList.EXTRA_ADDRESS); //receive the address of the bluetooth device

        new ConnectBT().execute(); //Call the class to connect

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.main_tab_content);

        mf = new _Manual();
        af = new _Automatic();

        viewPager.setOffscreenPageLimit(1);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        isBtConnected = true;

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                lv.setVisibility(View.INVISIBLE);
                tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorAccent));
                findViewById(R.id.main_tab_content).setVisibility(View.VISIBLE);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                lv.setVisibility(View.INVISIBLE);
                tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorAccent));
                findViewById(R.id.main_tab_content).setVisibility(View.VISIBLE);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                lv.setVisibility(View.INVISIBLE);
                tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorAccent));
                findViewById(R.id.main_tab_content).setVisibility(View.VISIBLE);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPageAdapter Adapter = new ViewPageAdapter(getSupportFragmentManager()); //to load the fragments
        Adapter.addFragments(mf, "Manual");
        Adapter.addFragments(af, "Automatic");
        viewPager.setAdapter(Adapter);
    }

    public void openAccessLog(View view) {

        fetchAccessLog();

        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.tabBack));
        findViewById(R.id.main_tab_content).setVisibility(View.INVISIBLE);
        lv.setVisibility(View.VISIBLE);
    }

    public class ViewPageAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> fragments = new ArrayList<>();
        ArrayList<String> tabT = new ArrayList<>();

        public ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragments(Fragment fragment, String titles) {
            this.fragments.add(fragment);
            this.tabT.add(titles);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public CharSequence getPageTitle(int position) {
            return tabT.get(position);
        }
    }

    public void logout(View view) {
        SharedPreferences sharedpref = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        sharedpref.edit().clear().commit();
        disconnect();
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute() {
            MySingleton.getInstance().showLoadingPopup(MainActivity.this, "Connecting...\nPlease wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try {
                if (btSocket == null || !isBtConnected) {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection

                    MySingleton.getInstance().setBluetoothSocket(btSocket);
                }
            } catch (IOException e) {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess) {
                msg("Connection Failed. Is it a SPP Bluetooth? Try again.");
                finish();
            } else {
                MySingleton.getInstance().beginListenForData();

                msg("Connected.");
            }
            MySingleton.getInstance().dismissLoadingPopup();
        }
    }

    private void msg(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }

    private void disconnect() {
        if (btSocket != null) //If the btSocket is busy
        {
            try {
                btSocket.close(); //close connection
            } catch (IOException e) {
                msg("Error");
            }
        }
        finish(); //return to the first layout

    }

    private void fetchAccessLog() {
        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray ans = jsonObject.getJSONArray("result");

                    dataAvailable(ans);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(request);
    }

    void dataAvailable(JSONArray ans) {
        sd.removeEntrys();
        JSONObject jobj;
        for (int i = 0; i < ans.length(); i++) {
            try {
                jobj = ans.getJSONObject(i);
                sd.addEntry(new ListData(jobj.getString("username"), jobj.getString("body"), jobj.getString("date"), jobj.getString("time")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private class SampleAdapter extends BaseAdapter {

        SampleAdapter() {
            mDataList = new ArrayList<ListData>();
        }

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public ListData getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void addEntry(ListData d) {
            if (!mDataList.contains(d)) {
                mDataList.add(d);
                notifyDataSetChanged();
            }
        }

        public void removeEntrys() {
            mDataList.clear();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getBaseContext(), R.layout.___list_item_acesslog, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final ListData item = getItem(position);

            if (item.userName.equals(""))
                item.userName = "Test";
            TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.userName.toUpperCase().charAt(0)), mColorGenerator.getColor(item.userName));
            holder.imageView.setImageDrawable(drawable);
            // UrlImageViewHelper.setUrlDrawable(holder.imageView, item.Path);

            holder.textUsername.setText(item.userName);
            holder.textUsername.setSelected(true);

            holder.textBody.setText(item.body);
            holder.textDate.setText(item.date);
            holder.textTime.setText(item.time);

            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;

        private ImageView imageView;
        private TextView textUsername;
        private TextView textBody;
        private TextView textDate;
        private TextView textTime;

        private ViewHolder(View view) {
            this.view = view;
            imageView = (ImageView) view.findViewById(R.id.imageViewdList);
            textUsername = (TextView) view.findViewById(R.id.textViewdList_Username);
            textBody = (TextView) view.findViewById(R.id.textViewdList_Body);
            textDate = (TextView) view.findViewById(R.id.textViewdList_Date);
            textTime = (TextView) view.findViewById(R.id.textViewdList_Time);
        }
    }

    private static class ListData {

        private String userName;
        private String body;
        private String date;
        private String time;

        public ListData(String uName, String body, String date, String time) {
            this.userName = uName;
            this.body = body;
            this.date = date;
            this.time = time;
        }
    }
}