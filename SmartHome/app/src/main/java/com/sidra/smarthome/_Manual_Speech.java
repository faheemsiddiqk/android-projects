package com.sidra.smarthome;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

import static android.widget.Toast.makeText;


/**
 * A fragment that launches other parts of the demo application.
 */
public class _Manual_Speech extends Fragment implements RecognitionListener {

    View v;

    /* Named searches allow to quickly reconfigure the decoder */
    private static final String KWS_SEARCH = "wakeup";

    private static final String LIGHT_ON_1 = "turn on the light";
    private static final String LIGHT_ON_2 = "switch on the light";
    private static final String LIGHT_ON_3 = "turn on the bulb";
    private static final String LIGHT_ON_4 = "switch on the bulb";

    private static final String LIGHT_OFF_1 = "turn off the light";
    private static final String LIGHT_OFF_2 = "switch off the light";
    private static final String LIGHT_OFF_3 = "turn off the bulb";
    private static final String LIGHT_OFF_4 = "switch off the bulb";

    private static final String fan_ON_1 = "turn on the fan";
    private static final String fan_ON_2 = "switch on the fan";

    private static final String fan_OFF_1 = "turn off the fan";
    private static final String fan_OFF_2 = "switch off the fan";

    private static final String FORECAST_SEARCH = "forecast";

    private static final String MENU_SEARCH = "menu";

    /* Keyword we are looking for to activate menu */
    private static final String KEYPHRASE = "hello smart home";

    /* Used to handle permission request */
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    private SpeechRecognizer recognizer;
    private HashMap<String, Integer> captions;

    SharedPreferences sharedpref;
    SharedPreferences.Editor editor;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.__fragment_manual_speech, container,
                false);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        sharedpref = getActivity().getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        sharedpref.edit().commit();
        editor = sharedpref.edit();

        captions = new HashMap<String, Integer>();
        captions.put(KWS_SEARCH, R.string.kws_caption);
        captions.put(MENU_SEARCH, R.string.menu_caption);
        captions.put(LIGHT_ON_1, R.string.digits_caption);
        captions.put(fan_ON_1, R.string.phone_caption);
        captions.put(FORECAST_SEARCH, R.string.forecast_caption);
        ((TextView) view.findViewById(R.id.caption_text))
                .setText("Preparing the recognizer");

        // Check if user has given permission to record audio
        int permissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);
            return;
        }
        ((Switch) view.findViewById(R.id.MainSwitch)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    runRecognizerSetup();
                else
                    onDestroy();
            }
        });
    }

    private void runRecognizerSetup() {
        // Recognizer initialization is a time-consuming and it involves IO,
        // so we execute it in async task
        new AsyncTask<Void, Void, Exception>() {
            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    Assets assets = new Assets(getContext());
                    File assetDir = assets.syncAssets();
                    setupRecognizer(assetDir);
                } catch (IOException e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception result) {
                if (result != null) {
                    ((TextView) v.findViewById(R.id.caption_text))
                            .setText("Failed to init recognizer " + result);
                } else {
                    switchSearch(KWS_SEARCH);
                }
            }
        }.execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                runRecognizerSetup();
            } else {
                getActivity().finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (recognizer != null) {
            recognizer.cancel();
            recognizer.shutdown();
        }
    }

    /**
     * In partial result we get quick updates about current hypothesis. In
     * keyword spotting mode we can react here, in other modes we need to wait
     * for final result in onResult.
     */
    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;

        String text = hypothesis.getHypstr();
        if (text.equals(KEYPHRASE)) {
            switchSearch(MENU_SEARCH);
        } else if (text.equals(LIGHT_ON_1) || text.equals(LIGHT_ON_2) || text.equals(LIGHT_ON_3) || text.equals(LIGHT_ON_4)) {
            if (MySingleton.getInstance().turnLed(true)) {
                editor.putBoolean("BulbStatus", true);
                editor.apply();
            } else
                MySingleton.getInstance().errorPopup(getActivity());
            //switchSearch(LIGHT_ON_1);
        } else if (text.equals(LIGHT_OFF_1) || text.equals(LIGHT_OFF_2) || text.equals(LIGHT_OFF_3) || text.equals(LIGHT_OFF_4)) {
            if (MySingleton.getInstance().turnLed(false)) {
                editor.putBoolean("BulbStatus", false);
                editor.apply();
            } else
                MySingleton.getInstance().errorPopup(getActivity());
            editor.putBoolean("BulbStatus", false);
            editor.apply();
            //switchSearch(LIGHT_ON_1);
        } else if (text.equals(fan_ON_1) || text.equals(fan_ON_2)) {
            if (MySingleton.getInstance().turnFan(true)) {
                editor.putBoolean("FanStatus", true);
                editor.apply();
            } else
                MySingleton.getInstance().errorPopup(getActivity());
            //switchSearch(fan_ON_1);
        } else if (text.equals(fan_OFF_1) || text.equals(fan_OFF_2)) {
            if (MySingleton.getInstance().turnLed(false)) {
                editor.putBoolean("FanStatus", false);
                editor.apply();
            } else
                MySingleton.getInstance().errorPopup(getActivity());
            //switchSearch(fan_ON_1);
        } else if (text.equals(FORECAST_SEARCH))
            switchSearch(FORECAST_SEARCH);
        else
            ((TextView) v.findViewById(R.id.result_text)).setText(text);
    }

    /**
     * This callback is called when we stop the recognizer.
     */
    @Override
    public void onResult(Hypothesis hypothesis) {
        ((TextView) v.findViewById(R.id.result_text)).setText("");
        if (hypothesis != null) {
            String text = hypothesis.getHypstr();
            makeText(getContext(), text, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBeginningOfSpeech() {
    }

    /**
     * We stop recognizer here to get a final result
     */
    @Override
    public void onEndOfSpeech() {
        if (!recognizer.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH);
    }

    private void switchSearch(String searchName) {
        recognizer.stop();

        // If we are not spotting, start listening with timeout (10000 ms or 10 seconds).
        if (searchName.equals(KWS_SEARCH))
            recognizer.startListening(searchName);
        else
            recognizer.startListening(searchName, 10000);

        String caption = getResources().getString(captions.get(searchName));
        ((TextView) v.findViewById(R.id.caption_text)).setText(caption);
    }

    private void setupRecognizer(File assetsDir) throws IOException {
        // The recognizer can be configured to perform multiple searches
        // of different kind and switch between them

        recognizer = SpeechRecognizerSetup.defaultSetup()
                .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))

                .setRawLogDir(assetsDir) // To disable logging of raw audio comment out this call (takes a lot of space on the device)

                .getRecognizer();
        recognizer.addListener(this);

        /** In your application you might not need to add all those searches.
         * They are added here for demonstration. You can leave just one.
         */

        // Create keyword-activation search.
        recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);

        // Create grammar-based search for selection between demos
        File menuGrammar = new File(assetsDir, "menu.gram");
        recognizer.addGrammarSearch(MENU_SEARCH, menuGrammar);

        // Create grammar-based search for digit recognition
        File digitsGrammar = new File(assetsDir, "digits.gram");
        recognizer.addGrammarSearch(LIGHT_ON_1, digitsGrammar);

        // Create language model search
        File languageModel = new File(assetsDir, "weather.dmp");
        recognizer.addNgramSearch(FORECAST_SEARCH, languageModel);

        // Phonetic search
        File phoneticModel = new File(assetsDir, "en-phone.dmp");
        recognizer.addAllphoneSearch(fan_ON_1, phoneticModel);
    }

    @Override
    public void onError(Exception error) {
        ((TextView) v.findViewById(R.id.caption_text)).setText(error.getMessage());
    }

    @Override
    public void onTimeout() {
        switchSearch(KWS_SEARCH);
    }
}