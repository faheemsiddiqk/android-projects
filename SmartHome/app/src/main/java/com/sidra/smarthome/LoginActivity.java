package com.sidra.smarthome;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText UserName,Password;

    private RequestQueue requestQueue;
    private static final String URL = "http://websystemsfahim.webddocsystems.com/Sidra/loginUser.php";
    private StringRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login); //R file has all the ids(Layout, mipmap etc)

        requestQueue = Volley.newRequestQueue(this);

        MySingleton.getInstance().isConnected(this);


        UserName = (EditText) findViewById(R.id.email); //(EditText) Type parsing
        Password = (EditText) findViewById(R.id.password);
    }


    public void openRegister(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    public void openDrawer(View view) {
        sendRequest(UserName.getText()+"",Password.getText()+"");
    }

    public void sendRequest(final String email, final String password) {
        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String ans = jsonObject.getString("result");

                    if (ans.contains("Welcome")) {
                        Toast.makeText(getApplicationContext(), ans, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(),DeviceList.class));
                        saveInfo(email,password);
                    } else {
                        Toast.makeText(getApplicationContext(), ans, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("email", email);
                hashMap.put("password", password);
                // Toast.makeText(getApplicationContext(),inputEmail.getText()+" "+inputPassword.getText()+" "+inputdupin.getText()+" "+inputlat.getText()+" "+inputFulllon.getText(),Toast.LENGTH_LONG).show();

                return hashMap;
            }
        };

        requestQueue.add(request);
    }

    public void saveInfo(String name, String pass) {
        MySingleton.getInstance().setUserId(name);

        SharedPreferences sharedpref = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        sharedpref.edit().clear().commit();
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString("UserName", name);
        editor.putString("Password", pass);
        editor.apply();
    }
}
