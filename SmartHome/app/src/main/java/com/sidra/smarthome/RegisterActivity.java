package com.sidra.smarthome;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegisterActivity extends AppCompatActivity {

    EditText UserName, Password, ConfirmPassword;

    private RequestQueue requestQueue;
    private static final String URL = "http://websystemsfahim.webddocsystems.com/Sidra/registerUser.php";
    private StringRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        requestQueue = Volley.newRequestQueue(this);

        UserName = (EditText) findViewById(R.id.email);
        Password = (EditText) findViewById(R.id.password);
        ConfirmPassword = (EditText) findViewById(R.id.cpassword);
    }

    public void registerUser(View view) {
        if ((Password.getText() + "").equals((ConfirmPassword.getText() + ""))) {
            final String email = UserName.getText() + "";
            final String password = Password.getText() + "";

            if (email.equals("") || password.equals("")) {
                new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Something Went Wrong !")
                        .setContentText("Empty Fields")
                        .show();
                return;
            }

            sendRequest(email, password);

            /*new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Registered Successfully")
                    .setContentText("Please proceed forward")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            saveInfo(email, password);
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            //pDialog.dismiss();
                            finish();
                        }
                    })
                    .show();*/
        } else
            new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something Went Wrong !")
                    .setContentText("Password mismatch")
                    .show();


    }


    public void openLogin(View view) {
        finish();
    }

    public void saveInfo(String name, String pass) {
        SharedPreferences sharedpref = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        sharedpref.edit().clear().commit();
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString("UserName", name);
        editor.putString("Password", pass);
        editor.apply();
    }

    public void sendRequest(final String email, final String password) {
        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String ans = jsonObject.getString("result");

                    if (ans.contains("Welcome") || ans.contains("Created")) {
                        Toast.makeText(getApplicationContext(), ans, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(),DeviceList.class));
                        MySingleton.getInstance().setUserId(email);
                        saveInfo(email,password);
                    } else {
                        Toast.makeText(getApplicationContext(), ans, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("email", email);
                hashMap.put("password", password);
                // Toast.makeText(getApplicationContext(),inputEmail.getText()+" "+inputPassword.getText()+" "+inputdupin.getText()+" "+inputlat.getText()+" "+inputFulllon.getText(),Toast.LENGTH_LONG).show();

                return hashMap;
            }
        };

        requestQueue.add(request);
    }
}
