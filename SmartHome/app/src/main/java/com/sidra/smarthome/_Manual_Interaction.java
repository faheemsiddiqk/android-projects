package com.sidra.smarthome;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A fragment that launches other parts of the demo application.
 */
public class _Manual_Interaction extends Fragment {

    ArrayList<ListData> main;
    ListView lv;

    ImageView im;
    RotateAnimation rotate;
    StartNextRotate startNext;

    SharedPreferences sharedpref;
    SharedPreferences.Editor editor;

    SampleAdapter sa;

    private boolean isViewFirstTime = true;

    public ArrayList<Boolean> status;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            if (isViewFirstTime) {
                isViewFirstTime = false;
                return;
            }
            status.clear();
            status.add(0, sharedpref.getBoolean("BulbStatus", false));
            status.add(1, sharedpref.getBoolean("FanStatus", false));
            status.add(2, sharedpref.getBoolean("LockStatus", false));
            sa.refresh();
        } else {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.__fragment_manual_interaction, container,
                false);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        sharedpref = getActivity().getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        sharedpref.edit().commit();
        editor = sharedpref.edit();

        status = new ArrayList<>();


        status.add(sharedpref.getBoolean("BulbStatus", false));
        status.add(sharedpref.getBoolean("FanStatus", false));
        status.add(sharedpref.getBoolean("LockStatus", false));

        main = new ArrayList<>();
        main.add(new ListData("Bulb", R.drawable.__bulb_off, R.drawable.__bulb_on, status.get(0)));
        main.add(new ListData("Fan", R.drawable.__fan, R.drawable.__fan, status.get(1)));
        main.add(new ListData("Lock", R.drawable.__unlocked, R.drawable.__locked, status.get(2)));

        sa = new SampleAdapter();

        lv = view.findViewById(R.id.interation_listView);
        lv.setAdapter(sa);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String name = main.get(i).Description;
                ImageView im_ = view.findViewById(R.id.list_item_image);

                if (name.equals("Bulb")) {
                    main.get(i).status = !main.get(i).status;
                    if (MySingleton.getInstance().turnLed(main.get(i).status)) {    // From Here -> MySingleton.turnLed -> turnOnled, tunrnOffled  -> if(true,false)
                        im_.setImageResource(main.get(i).status
                                ? R.drawable.__bulb_on : R.drawable.__bulb_off);
                        editor.putBoolean("BulbStatus", main.get(i).status);
                        editor.apply();
                    }else
                        MySingleton.getInstance().errorPopup(getActivity());
                } else if (name.equals("Fan")) {
                    main.get(i).status = !main.get(i).status;
                    Boolean a = main.get(i).status;
                    if (MySingleton.getInstance().turnFan(a)) {
                        if (!a) {
                            im.clearAnimation();
                        } else {
                            im = view.findViewById(R.id.list_item_image);
                            im.startAnimation(rotate);
                        }
                        editor.putBoolean("FanStatus", a);
                        editor.apply();
                    }else
                        MySingleton.getInstance().errorPopup(getActivity());
                } else if (name.equals("Lock")) {
                    main.get(i).status = !main.get(i).status;
                    if (MySingleton.getInstance().turnLock(main.get(i).status)) {
                        im_.setImageResource(main.get(i).status
                                ? R.drawable.__locked : R.drawable.__unlocked);
                        editor.putBoolean("LockStatus", main.get(i).status);
                        editor.apply();
                    }else
                        MySingleton.getInstance().errorPopup(getActivity());
                }
            }
        });

        rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(1000);
        rotate.setRepeatMode(Animation.INFINITE);
        startNext = new StartNextRotate();
        rotate.setAnimationListener(startNext);
    }

    private class StartNextRotate implements Animation.AnimationListener {

        public void onAnimationEnd(Animation animation) {
            // TODO Auto-generated method stub
            if (sharedpref.getBoolean("FanStatus", false))
                im.startAnimation(rotate);
            else
                im.clearAnimation();
        }

        public void onAnimationRepeat(Animation animation) {
            // TODO Auto-generated method stub

        }

        public void onAnimationStart(Animation animation) {
            // TODO Auto-generated method stub

        }

    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void refresh() {
            main.get(0).status = status.get(0);
            main.get(1).status = status.get(1);
            main.get(2).status = status.get(2);
            notifyDataSetChanged();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.___list_item_interaction, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);

            holder.Description.setText(item.Description);
            if (item.Description.equals("Fan")) {
                im = convertView.findViewById(R.id.list_item_image);
                holder.Image.setImageResource(item.ID_on);
                if (item.status)
                    holder.Image.startAnimation(rotate);
                else
                    holder.Image.clearAnimation();
            } else {
                if (item.status)
                    holder.Image.setImageResource(item.ID_on);
                else
                    holder.Image.setImageResource(item.ID_off);
            }
            return convertView;
        }
    }

    private static class ViewHolder {
        private View view;

        private TextView Description;
        private ImageView Image;

        private ViewHolder(View view) {
            this.view = view;
            Description = view.findViewById(R.id.list_item_name);
            Image = view.findViewById(R.id.list_item_image);
        }
    }


    private class ListData {
        private String Description;
        private int ID_off;
        private int ID_on;
        boolean status;

        public ListData(String name, int res_id_off, int res_id_on, boolean status) {
            this.Description = name;
            this.ID_off = res_id_off;
            this.ID_on = res_id_on;
            this.status = status;
        }
    }
}