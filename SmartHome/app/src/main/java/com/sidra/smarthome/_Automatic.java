package com.sidra.smarthome;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;


/**
 * A fragment that launches other parts of the demo application.
 */
public class _Automatic extends Fragment {

    View v;
    Switch Main;
    TextView Temperature, Motion;
    static TextView Temperature_,Motion_;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.__fragment_automatic, container,
                false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Main = view.findViewById(R.id.switchMain);
        Temperature = view.findViewById(R.id.TempTV);
        Motion = view.findViewById(R.id.MotionTV);

        Temperature_ = Temperature;
        Motion_ = Motion;

        Main.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    MySingleton.getInstance().AutomaticMode(true);
                else
                    MySingleton.getInstance().AutomaticMode(false);
            }
        });
    }

    public static void changeValue_T(String t){
        Temperature_.setText(t);
    }
    public static void changeValue_M(String m){
        Motion_.setText(m);
    }
}