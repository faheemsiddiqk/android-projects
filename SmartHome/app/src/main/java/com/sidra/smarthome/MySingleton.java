package com.sidra.smarthome;

import android.app.Activity;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by faheem on 03/07/2017.
 */

public class MySingleton extends AppCompatActivity {

    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    int counter;
    volatile boolean stopWorker;


    static Context Contextt_;

    private String userId = "";

    private static final MySingleton ourInstance = new MySingleton();
    private SweetAlertDialog pDialog = null;

    SharedPreferences sharedPreferences;
    BluetoothSocket btSocket = null;

    private RequestQueue requestQueue;
    private static final String URL = "http://websystemsfahim.webddocsystems.com/Sidra/accessLogEntry.php";
    private StringRequest request;

    public static MySingleton getInstance() {
        return ourInstance;
    }

    private MySingleton() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String u) {
        userId = u;
    }

    public boolean isConnected(Context context) {
        Contextt_ = context;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifiInfo != null && wifiInfo.isConnected()) || (mobileInfo != null && mobileInfo.isConnected())) {
            return true;
        } else {
            showDialog(context);
            return false;
        }
    }

    public boolean isConnectedButton(Context c) {
        ConnectivityManager connectivityManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifiInfo != null && wifiInfo.isConnected()) || (mobileInfo != null && mobileInfo.isConnected())) {
            return true;
        } else {
            return false;
        }
    }

    private void showDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Connect to a network or quit")
                .setCancelable(false)
                .setPositiveButton("Connect to WIFI/Refresh", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (!isConnected(context))
                            context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                })
                .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((Activity) context).finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showLoadingPopup(Context con, String a) {
        Contextt_ = con;
        pDialog = new SweetAlertDialog(con, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#cb3439"));
        pDialog.setTitleText(a);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void dismissLoadingPopup() {
        if (pDialog != null)
            try {
                pDialog.dismiss();
            } catch (IllegalArgumentException e) {
                return;
            }
    }

    public void errorPopup(Context c) {
        new SweetAlertDialog(c, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Something Went Wrong !")
                .setContentText("Unable to send request")
                .show();
    }

    public void setBluetoothSocket(BluetoothSocket bt) {
        btSocket = bt;
    }

    public boolean turnLed(boolean a) {
        if (a)
            return turnOnLed();
        else
            return turnOffLed();
    }

    public boolean turnFan(boolean a) {
        if (a)
            return turnOnFan();
        else
            return turnOffFan();
    }

    public boolean turnLock(boolean a) {
        if (a)
            return false;
        else
            return false;
    }

    public boolean AutomaticMode(boolean a){
        if(a)
            return turnOnAutomaticM();
        else
            return turnOffAutomaticM();
    }

    private boolean turnOffLed() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("20\n".toString().getBytes());
                logEntry("Switched off The Led on Pin # 2");
                return true;
            } catch (IOException e) {
                System.out.println("Error");
                return false;
            }
        }
        return false;
    }

    private boolean turnOnLed() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("21\n".toString().getBytes());
                logEntry("Switched on The Led on Pin # 2");
                return true;
            } catch (IOException e) {
                System.out.println("Error");
                return false;
            }
        }
        return false;
    }

    private boolean turnOffFan() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("30\n".toString().getBytes());
                logEntry("Switched off The Fan on Pin # 3");
                return true;
            } catch (IOException e) {
                System.out.println("Error");
                return false;
            }
        }
        return false;
    }

    private boolean turnOnFan() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("31\n".toString().getBytes());
                logEntry("Switched on The Fan on Pin # 3");
                return true;
            } catch (IOException e) {
                System.out.println("Error");
                return false;
            }
        }
        return false;
    }

    private boolean turnOffAutomaticM() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("AF\n".toString().getBytes());
                logEntry("Switched off The AUTOMATIC MODE");
                return true;
            } catch (IOException e) {
                System.out.println("Error");
                return false;
            }
        }
        return false;
    }

    private boolean turnOnAutomaticM() {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write("AO\n".toString().getBytes());
                logEntry("Switched on The AUTOMATIC MODE");
                return true;
            } catch (IOException e) {
                System.out.println("Error");
                return false;
            }
        }
        return false;
    }

    private void logEntry(final String text){
        requestQueue = Volley.newRequestQueue(Contextt_);

        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String ans = jsonObject.getString("result");

                    if (ans.equals("Entry Made")) {
                        Toast.makeText(Contextt_, ans, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Contextt_, ans, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
                String formattedDate = df.format(c.getTime());

                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("time", formattedDate.split(" ")[1]+" "+formattedDate.split(" ")[2]);
                hashMap.put("date", formattedDate.split(" ")[0]);
                hashMap.put("user", userId);
                hashMap.put("text", text);
                // Toast.makeText(getApplicationContext(),inputEmail.getText()+" "+inputPassword.getText()+" "+inputdupin.getText()+" "+inputlat.getText()+" "+inputFulllon.getText(),Toast.LENGTH_LONG).show();

                return hashMap;
            }
        };

        requestQueue.add(request);
    }





    public void beginListenForData()
    {
        try {
            mmInputStream = btSocket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        final boolean[] stopWorker = {false};
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopWorker[0])
                {
                    try
                    {
                        int bytesAvailable = mmInputStream.available();
                        if(bytesAvailable > 0)
                        {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++)
                            {
                                byte b = packetBytes[i];
                                if(b == delimiter)
                                {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable()
                                    {
                                        public void run()
                                        {
                                            if(data.equals("Motion detected!\r") || data.equals("Motion stopped!\r"))
                                                _Automatic.changeValue_M("Motion Sensor : "+data);
                                            else
                                                _Automatic.changeValue_T("Temperature Sensor : "+data);
                                        }
                                    });
                                }
                                else
                                {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    catch (IOException ex)
                    {
                        stopWorker[0] = true;
                    }
                }
            }
        });

        workerThread.start();
    }
}