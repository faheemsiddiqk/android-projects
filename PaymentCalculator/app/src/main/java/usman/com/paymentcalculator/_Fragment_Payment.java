package usman.com.paymentcalculator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A fragment that launches other parts of the demo application.
 */
public class _Fragment_Payment extends Fragment {
    String[] individuals;
    boolean topVal;

    TextView tv_top, am_top, tv_btm, am_btm;
    static TextView _tv_top_, _am_top_, _tv_btm_, _am_btm_;

    TextInputLayout inputLayoutAmount, inputLayoutDetail;
    EditText inputAmount, inputDetail;

    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.__fragment_payment, container,
                false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        individuals = getResources().getStringArray(R.array.arrayIndividuals);

        tv_top = (TextView) view.findViewById(R.id.tv_top);
        _tv_top_ = tv_top;
        am_top = (TextView) view.findViewById(R.id.am_top);
        _am_top_ = am_top;
        tv_btm = (TextView) view.findViewById(R.id.tv_btm);
        _tv_btm_ = tv_btm;
        am_btm = (TextView) view.findViewById(R.id.am_btm);
        _am_btm_ = am_btm;

        topVal = true;
        for (int i = 0; i < individuals.length; i++) {

            if (!individuals[i].equals(MySingleton.getInstance().getUserName()) && topVal) {
                tv_top.setText("Payable to " + individuals[i] + " : ");
//                am_top.setText(MySingleton.getInstance().fetch_Top(individuals[i]) + " $");
                topVal = false;
                i++;
            }
            if (!individuals[i].equals(MySingleton.getInstance().getUserName())) {
                try {
                    tv_btm.setText("Payable to " + individuals[i] + " : ");
                } catch (IndexOutOfBoundsException e) {

                }
                //
            }
        }

        inputLayoutAmount = (TextInputLayout) view.findViewById(R.id.input_layout_amount);
        inputAmount = (EditText) view.findViewById(R.id.input_amount);

        inputLayoutDetail = (TextInputLayout) view.findViewById(R.id.input_layout_detail);
        inputDetail = (EditText) view.findViewById(R.id.input_detail);

        view.findViewById(R.id.submitPayment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String amount = inputAmount.getText().toString();
                String detail = inputDetail.getText().toString();
                if (amount.equals("")) {
                    inputLayoutAmount.setError("Please enter an amount");
                } else {
                    inputLayoutAmount.setErrorEnabled(false);
                }
                if (detail.equals("")) {
                    inputLayoutDetail.setError("Please enter some info");
                } else {
                    inputLayoutDetail.setErrorEnabled(false);
                }
                MySingleton.getInstance().sendFirebasePD(amount, v);
                MySingleton.getInstance().sendFirebaseDesc(detail, amount);

                inputAmount.setText("");
                inputDetail.setText("");

                // Check if no view has focus:
                View view_ = getActivity().getCurrentFocus();
                if (view_ != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        });

        view.findViewById(R.id.buttonL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySingleton.getInstance().doTransitiveProperty();
                logout();
            }
        });
    }

    private void logout() {
        SharedPreferences sharedpref = getActivity().getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        sharedpref.edit().clear().apply();

        getActivity().finish();
        MySingleton.destroy();
        startActivity(new Intent(getActivity(), LoginActivity.class));
    }

    public static void dataAvailable(String s1, String s2) {
        if (_am_top_ != null)
            _am_top_.setText("$ " + s1);
        if (_am_btm_ != null)
            _am_btm_.setText("$ " + s2);
    }

    public static void dataChanged(String t, String b) {
        if (_tv_top_.getText().toString().contains(t.split("_")[1]))
            _am_top_.setText(b + " $");
        if (_tv_btm_.getText().toString().contains(t.split("_")[1]))
            _am_btm_.setText(b + " $");
    }
}