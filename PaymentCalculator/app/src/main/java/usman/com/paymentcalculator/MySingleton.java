package usman.com.paymentcalculator;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


/**
 * Created by faheem on 03/07/2017.
 */

public class MySingleton extends AppCompatActivity {

    private static MySingleton ourInstance;
    private String userName = "";
    private String top_name, bottom_name;
    private Object top_amount, bottom_amount;

    SimpleDateFormat mFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.US);
    FirebaseDatabase database;
    DatabaseReference myRefPD, myRefTM;

    String[] individuals;

    public static void renew() {
        ourInstance = new MySingleton();
    }

    public static MySingleton getInstance() {
        return ourInstance;
    }

    public static void destroy() {
        ourInstance = null;
    }

    public void setUserName(Context ctx, String un) {
        userName = un;
        individuals = ctx.getResources().getStringArray(R.array.arrayIndividuals);

        database = FirebaseDatabase.getInstance();
        myRefPD = database.getReference("Payment's Due");
        myRefTM = database.getReference("Transaction's Made");

        getPayables();
    }

    public String getUserName() {
        return userName;
    }

    public void sendFirebasePD(String amount, View v) {

        final float ammount = Float.parseFloat(amount);
        final float final_ = (ammount) / 3;

        myRefPD.child(userName).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                float value1 = Float.parseFloat(dataSnapshot.child("amount_" + top_name).getValue().toString());
                if (value1 != 0f) {
                    value1 = value1 - final_;
                    if (value1 < 0) {
                        myRefPD.child(top_name).child("amount_" + userName).setValue(round(Math.abs(value1), 2));
                        myRefPD.child(userName).child("amount_" + top_name).setValue(0);
                    } else {
                        myRefPD.child(userName).child("amount_" + top_name).setValue(round(Math.abs(value1), 2));
                        myRefPD.child(top_name).child("amount_" + userName).setValue(0);
                    }
                } else {
                    myRefPD.child(top_name).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot dataSnapshot) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    float value = Float.parseFloat(dataSnapshot.child("amount_" + userName).getValue().toString());
                                    value = value + final_;
                                    myRefPD.child(top_name).child("amount_" + userName).setValue(round(Math.abs(value), 2));
                                }
                            }, 1500);

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                float value2 = Float.parseFloat(dataSnapshot.child("amount_" + bottom_name).getValue().toString());
                if (value2 != 0f) {
                    value2 = value2 - final_;
                    if (value2 < 0) {
                        myRefPD.child(bottom_name).child("amount_" + userName).setValue(round(Math.abs(value2), 2));
                        myRefPD.child(userName).child("amount_" + bottom_name).setValue(0);
                    } else {
                        myRefPD.child(userName).child("amount_" + bottom_name).setValue(round(Math.abs(value2), 2));
                        myRefPD.child(bottom_name).child("amount_" + userName).setValue(0);
                    }
                } else {
                    myRefPD.child(bottom_name).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot dataSnapshot) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    float value = Float.parseFloat(dataSnapshot.child("amount_" + userName).getValue().toString());
                                    value = value + final_;
                                    myRefPD.child(bottom_name).child("amount_" + userName).setValue(round(value, 2));
                                }
                            }, 3000);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                float value3 = Float.parseFloat(dataSnapshot.child("amount_" + "total").getValue().toString());
                value3 += ammount;
                myRefPD.child(userName).child("amount_" + "total").setValue(round(value3, 2));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        /*if (top_name == null)
            top_name = ((TextView) v.findViewById(R.id.tv_top)).getText().toString().split(" ")[2];

        if (bottom_name == null)
            bottom_name = ((TextView) v.findViewById(R.id.tv_btm)).getText().toString().split(" ")[2];*/

    }

    public void sendFirebaseDesc(String description, String amount) {

        Map notification = new HashMap<>();
        notification.put("amount", round(Float.parseFloat(amount), 2));
        notification.put("by", userName);
        notification.put("detail", description);

        String timestamp = null;
        try {
            Date a = mFormatter.parse(Calendar.getInstance().getTime().toString());
            timestamp = a + "";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        notification.put("timestamp", timestamp);

        myRefTM.push().setValue(notification);
    }

    public double round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        double a = bd.doubleValue();
        return a;
    }

    private void getPayables() {
        myRefPD.child(userName).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i = 0;
                Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                int length = (int) dataSnapshot.getChildrenCount();
                top_amount = bottom_amount = null;

                while (i < length) {
                    String key = iterator.next().getKey().toString();
                    if (key.contains("amount_")) {
                        if (top_amount == null) {
                            top_name = key.split("_")[1];
                            top_amount = dataSnapshot.child(key).getValue();
                            key = iterator.next().getKey().toString();
                            i++;
                        }
                        if (bottom_amount == null) {
                            bottom_name = key.split("_")[1];
                            bottom_amount = dataSnapshot.child(key).getValue();
                            key = iterator.next().getKey().toString();
                            i++;
                        }
                    }
                    i++;
                }

                _Fragment_Payment.dataAvailable(top_amount.toString(), bottom_amount.toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void doTransitiveProperty() {
        myRefPD.getParent().addListenerForSingleValueEvent(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean a = false;
                HashMap<String, HashMap> main = (HashMap<String, HashMap>) dataSnapshot.child("Payment's Due").getValue();

                String key = "", c_value = "";
                float outerValue = 0f, innerValue = 0f, remainderValue = 0f;
                HashMap<String, Object> value_o, value_i;
                value_o = main.get(userName);
                for (String key_o : value_o.keySet()) {
                    if (key_o.contains("total")) {
                    } else {
                        //Double d = Double.parseDouble(value_o.get(key_o));
                        outerValue = Float.parseFloat(value_o.get(key_o).toString());
                        if (outerValue > 0) {
                            // A -> B
                            Log.i("123", "onDataChange: " + key_o + " " + value_o.get(key_o));
                            key = key_o.split("_")[1];
                            //break;
                            value_i = main.get(key);
                            for (String key_i : value_i.keySet()) {
                                if (key_i.contains("total") || key_i.contains(userName)) {
                                } else {
                                    // B -> C
                                    Log.i("123", "onDataChange: " + key_i + " " + value_i.get(key_i));
                                    innerValue = Float.parseFloat(value_i.get(key_i).toString());
                                    Log.i("123", "formula is : " + outerValue + " - " + innerValue);
                                    remainderValue = Math.abs(outerValue - innerValue);
                                    Log.i("123", "remainder is : " + remainderValue);
                                    c_value = key_i;
                                    if (outerValue <= innerValue) {
                                        myRefPD.child(userName).child("amount_" + key).setValue(round(0, 2));
                                        float fromDB = Float.parseFloat(main.get(userName).get(c_value).toString());
                                        myRefPD.child(userName).child(c_value).setValue(round(outerValue + fromDB, 2));

                                        myRefPD.child(key).child(c_value).setValue(round(remainderValue, 2));
                                    } else {
                                        myRefPD.child(userName).child("amount_" + key).setValue(round(remainderValue, 2));
                                        float fromDB = Float.parseFloat(main.get(userName).get(c_value).toString());
                                        myRefPD.child(userName).child(c_value).setValue(round(innerValue + fromDB, 2));

                                        myRefPD.child(key).child(c_value).setValue(round(0, 2));
                                    }

                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}



