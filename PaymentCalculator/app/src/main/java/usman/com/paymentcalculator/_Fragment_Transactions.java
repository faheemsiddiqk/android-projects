package usman.com.paymentcalculator;


import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;


import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * A fragment that launches other parts of the demo application.
 */
public class _Fragment_Transactions extends Fragment {

    Spinner sDoctor, sServiceFee;
    LinearLayout lDateTo, lDateFrom;
    TextView dateto, datefrom;

    private int mYear, mMonth, mDay;

    String[] Months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    ArrayList<ListData> mDataList;
    ArrayList Spinner;
    ListView lv;
    LinearLayout tb;
    JSONArray jdr;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.__fragment_transactions, container,
                false);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        sDoctor = (Spinner) view.findViewById(R.id.spinner_Doctors);
        sServiceFee = (Spinner) view.findViewById(R.id.spinner_ServiceFee);
        lDateTo = (LinearLayout) view.findViewById(R.id.openDateTo);
        lDateFrom = (LinearLayout) view.findViewById(R.id.openDateFrom);
        dateto = (TextView) view.findViewById(R.id.tv_DateTo);
        datefrom = (TextView) view.findViewById(R.id.tv_DateFrom);


        lv = (ListView) view.findViewById(R.id.listView);
        tb = (LinearLayout) view.findViewById(R.id.TableYesDate);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        dateto.setText(mYear + "-" + Months[mMonth] + "-" + mDay);
        datefrom.setText(2017 + "-" + Months[4] + "-" + 1);

        mDataList = new ArrayList<ListData>();
        Spinner = new ArrayList();

        lDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                dateto.setText(year + "-" + Months[monthOfYear] + "-" + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        lDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                datefrom.setText(year + "-" + Months[monthOfYear] + "-" + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        Spinner.add("- All -");


        ArrayAdapter<String> adapterD = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, Spinner);
        adapterD.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sDoctor.setAdapter(adapterD);

        view.findViewById(R.id.fetchReportDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#cb3439"));
                pDialog.setTitleText("Fetching - Please Wait");
                pDialog.setCancelable(false);
                pDialog.show();

                mDataList.clear();
                lv.setAdapter(new SampleAdapter());
                tb.setVisibility(View.INVISIBLE);

                String dFrom = datefrom.getText() + "";
                String dTo = dateto.getText() + "";

                int dIndex = sDoctor.getSelectedItemPosition();
                dIndex--;

                //JSONArray jdr = wcf.GetJsonResult("DoctorList");
                String dID = "";
                if (dIndex == -1)
                    dID = "-1";
                else {
                    try {
                        dID = jdr.getJSONArray(dIndex).get(3) + "";
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                String serFee;
                if (sServiceFee.getSelectedItemPosition() != 0)
                    serFee = (sServiceFee.getSelectedItem() + "").split(" ")[0];
                else
                    serFee = "-1";



            }
        });

    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public ListData getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.___list_item_pentadruple, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

           /* TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.textView.setText(item.Name);*/

            String[] parts = item.Date.split("-");

            holder.tvDate.setText(parts[0] + "\n- " + parts[1] + " -\n" + parts[2]);
            holder.tvDoctor.setText(item.Doctor);
            holder.tvConsultationType.setText(item.Consultation_Type);
            holder.tvPaymentMethod.setText(item.Payment_Method);
            holder.tvCharges.setText(item.Charges);

            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;

        private TextView tvDate;
        private TextView tvDoctor;
        private TextView tvConsultationType;
        private TextView tvPaymentMethod;
        private TextView tvCharges;

        private ViewHolder(View view) {
            this.view = view;
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvDoctor = (TextView) view.findViewById(R.id.tvDoctor);
            tvConsultationType = (TextView) view.findViewById(R.id.tvConsultationType);
            tvPaymentMethod = (TextView) view.findViewById(R.id.tvPaymentMethod);
            tvCharges = (TextView) view.findViewById(R.id.tvCharges);
        }
    }

    private static class ListData {

        private String ID;
        private String Date;
        private String Doctor;
        private String Consultation_Type;
        private String Payment_Method;
        private String Charges;

        public ListData(String ID, String D, String Doc, String CT, String PM, String C) {
            this.ID = ID;
            this.Date = D;
            this.Doctor = Doc;
            this.Consultation_Type = CT;
            this.Payment_Method = PM;
            this.Charges = C;
        }
    }
}