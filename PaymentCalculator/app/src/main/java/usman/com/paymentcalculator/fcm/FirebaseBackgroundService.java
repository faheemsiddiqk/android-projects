package usman.com.paymentcalculator.fcm;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import usman.com.paymentcalculator.MySingleton;
import usman.com.paymentcalculator.R;
import usman.com.paymentcalculator._Fragment_Payment;

/**
 * Created by faheem on 04/12/2017.
 */

public class FirebaseBackgroundService extends Service {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRefPD = database.getReference("Payment's Due");
    DatabaseReference myRefTM = database.getReference("Transaction's Made");
    String[] individuals;
    private Long top, bottom;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Read from the database
        Query query = myRefPD.child(userName());

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d("", "added");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d("", "changed");
                String t = dataSnapshot.getKey(), b = dataSnapshot.getValue().toString();
                //_Fragment_Payment.dataChanged(t,b);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("", "removed");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d("", "moved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("", "cancelled");
            }
        });

        /*// Read from the database
        Query _query_ = myRefTM.orderByChild("sender").equalTo("system@webdoc.com.pk");

        _query_.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d("", "added");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d("", "changed");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("", "removed");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d("", "moved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("", "cancelled");
            }
        });*/

    }


    private String userName() {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        String uname = sharedpref.getString("UserName", "0");

        return uname;
    }

}
