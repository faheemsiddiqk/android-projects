package usman.com.paymentcalculator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import usman.com.paymentcalculator.fcm.FirebaseBackgroundService;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    TextInputLayout inputLayoutLogin;
    EditText inputLogin;

    Button bLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*startService(new Intent(getApplicationContext(), FirebaseBackgroundService.class));*/

        checkInfo();

        inputLayoutLogin = (TextInputLayout) findViewById(R.id.input_layout_amount);
        inputLogin = (EditText) findViewById(R.id.input_amount);

        findViewById(R.id.submitLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String _name_ = inputLogin.getText().toString();
                String name = _name_.substring(0, 1).toUpperCase() + _name_.substring(1);
                if (name.equals("")) {
                    inputLayoutLogin.setError("Empty Field");
                } else {
                    inputLayoutLogin.setErrorEnabled(false);
                    saveInfo(name);
                    checkInfo();
                }
            }
        });
    }

    public void saveInfo(String name) {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString("UserName", name);
        editor.apply();
    }

    public void checkInfo() {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        String userName = sharedpref.getString("UserName", "0");

        if (!userName.equals("0")) {
            MySingleton.renew();
            MySingleton.getInstance().setUserName(getApplicationContext(),userName);
            //getSinchServiceInterface().startClient(userName);
            startActivity(new Intent(getApplicationContext(), __MainActivity.class));
        }
    }

}
